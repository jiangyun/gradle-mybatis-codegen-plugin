# ${title}数据库设计文档

${description}

备注：
- PRI: 主键
- UNI: 唯一值
- MUL: 普通索引

<#list tableList as table>
### ${table_index + 1}. ${table.comment}
#### 表名：${table.tableName}
#### 存储引擎(engine)：${table.engine}
#### 排序规则(collate)：${table.collation}

|序号|字段名|字段类型|完整类型|是否为空|默认值|索引类型|说明|
|:---:|:---:|:---:|:---:|:---:|:---:|:---:|:---:|:----|
<#list table.columns as column>
| ${column_index + 1} | ${column.columnName} | ${column.dataType} | ${column.columnType} | ${column.nullFlag} | ${column.columnDefault} | ${column.columnKey} | ${column.columnComment} |
</#list>
##### SQL语句:
```sql
-- DROP TABLE IF EXISTS `${table.tableName}`;
CREATE TABLE IF NOT EXISTS `${table.tableName}`(
    <#list table.columns as column>
    `${column.columnName}` ${column.columnType}<#if "${column.nullFlag}"=="YES"> DEFAULT NULL<#else><#if '${column.nullFlag}'=="NO"> NOT NULL <#else> NOT NULL DEFAULT ${column.columnDefault}</#if></#if> ${column.extra} COMMENT '${column.columnComment}',
    </#list>
) ENGINE = ${table.engine}
  DEFAULT CHARSET = ${table.defaultCharset}
  COLLATE = ${table.collation} ${table.rowFormat} COMMENT ='${table.comment}';
```
</#list>