${r'<?xml version="1.0" encoding="UTF-8" ?>'}
${r'<!DOCTYPE mapper PUBLIC "-//mybatis.org//DTD Mapper 3.0//EN" "http://mybatis.org/dtd/mybatis-3-mapper.dtd" >'}

${r'<'}mapper namespace="${basePackage}.${mapper}.${className}Mapper">
    <!-- 结果映射关系 -->
    ${r'<'}resultMap id="AllColumnMap" type="${basePackage}.vo.${className}VO">
<#list columns as column>
    <#if (column.attrName == "deleted")>
    <#else>
        ${r'<'}result column="${column.columnName}" property="${column.attrName}"<#if (column.columnType == "json")> typeHandler="com.baomidou.mybatisplus.extension.handlers.FastjsonTypeHandler"</#if>/>
    </#if>
</#list>
    ${r'<'}/resultMap>

    <!-- 表字段的sql -->
    ${r'<'}sql id="all_column">
    <#list columns as column>
        <#if (column.attrName == "deleted")>
        <#else>
        ${column.columnName}<#if column_has_next>,</#if>
        </#if>
    </#list>
    ${r'<'}/sql>

    <!-- 查询列表 -->
    ${r'<'}select id="findPage" resultType="${basePackage}.vo.${className}VO">
        SELECT
        <#list columns as column>
            <#if (column.attrName == "deleted")>
            <#else>
            t1.${column.columnName}<#if column_has_next>,</#if>
            </#if>
        </#list>
        FROM ${tableName} t1
        ${r'<'}where>
        <#list columns as column>
            ${r'<'}if test="param.${column.attrName} != null"> AND t1.${column.columnName} = ${r'#'}{param.${column.attrName}}${r'<'}/if>
        </#list>
            <#if haveLogicDelField>
            AND t1.deleted = 0
            </#if>
        ${r'<'}/where>
        ORDER BY<#list pkColumns as column> t1.${column.columnName}<#if column_has_next>, </#if></#list> DESC
    ${r'<'}/select>

    <!-- 查询一条记录 -->
    ${r'<'}select id="findByPrimaryKey" resultType="${basePackage}.vo.${className}VO">
        SELECT
        <#list columns as column>
            t1.${column.columnName}<#if column_has_next>,</#if>
        </#list>
        FROM ${tableName} t1
        ${r'<'}where>
        <#list pkColumns as column>
            <#if column_index == 0>
            t1.${column.columnName} = ${r'#'}{${column.attrName}}
            <#else>
            AND t1.${column.columnName} = ${r'#'}{${column.attrName}}
            </#if>
        </#list>
            <#if haveLogicDelField>
            AND t1.deleted = 0
            </#if>
        ${r'<'}/where>
    ${r'<'}/select>

${r'<'}/mapper>