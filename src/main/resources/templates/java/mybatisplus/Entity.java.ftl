package ${basePackage}.${entity};

import com.baomidou.mybatisplus.annotation.*;
import com.baomidou.mybatisplus.extension.handlers.FastjsonTypeHandler;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import java.io.Serializable;
<#if haveBigDecimal>
import java.math.BigDecimal;
</#if>
<#if haveLocalDate>
import java.time.LocalDate;
</#if>
<#if haveLocalDateTime>
import java.time.LocalDateTime;
</#if>
<#if haveLocalTime>
import java.time.LocalTime;
</#if>

/**
 * ${comment}表(与数据库表字段一一对应对象)
 *
 * @author ${author}
 * @since ${createTime}
 */
@TableName(value = "${tableName}"<#if havaJsonData>, autoResultMap = true</#if>)
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class ${className}Entity implements Serializable {
    private static final long serialVersionUID = 1L;
<#list columns as column>
    /**
     * ${column.columnComment}
     */
    <#if (column.columnKey == "PRI")>
        <#if (pkColumns?size > 1)>
    @TableField(value = "${column.columnName}")
        <#else>
    @TableId(value = "${column.columnName}", type = IdType.NONE)
        </#if>
    <#else>
        <#if (column.attrName == "createBy" || column.attrName =="createUser" || column.attrName == "createAt" || column.attrName == "createTime" || column.attrName == "createDate" || column.attrName == "valid" || column.attrName == "deleted")>
    @TableField(value = "${column.columnName}", fill = FieldFill.INSERT)
        <#elseif  (column.attrName == "updateBy" || column.attrName == "updateUser" || column.attrName == "updateAt" || column.attrName == "updateTime" || column.attrName == "updateDate")>
    @TableField(value = "${column.columnName}", fill = FieldFill.INSERT_UPDATE)
        <#else>
    @TableField(value = "${column.columnName}"<#if (column.columnType == "json")> , typeHandler = FastjsonTypeHandler.class</#if>)
        </#if>
    </#if>
    private ${column.attrType} ${column.attrName};
</#list>
}
