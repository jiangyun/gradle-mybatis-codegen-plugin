package ${basePackage}.vo;

<#if enableGenerateDoc>
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
</#if>
import lombok.Data;
import lombok.experimental.Accessors;
import java.io.Serializable;
<#if haveBigDecimal>
import java.math.BigDecimal;
</#if>
<#if haveLocalDate>
import java.time.LocalDate;
</#if>
<#if haveLocalDateTime>
import java.time.LocalDateTime;
</#if>
<#if haveLocalTime>
import java.time.LocalTime;
</#if>

/**
 * ${comment}
 *
 * @author ${author}
 * @since ${createTime}
 */
<#if enableGenerateDoc>
@ApiModel(value = "${comment}返回信息")
</#if>
@Data
@Accessors(chain = true)
public class ${className}VO implements Serializable {
    private static final long serialVersionUID = 1L;
    <#list columns as column>
    <#if (column.attrName == "deleted")>
    <#else>
    /**
     * ${column.columnComment}
     */
    <#if enableGenerateDoc>
    @ApiModelProperty(value = "${column.columnComment}", dataType = "${column.fullAttrType}", example = "${column.example}")
    </#if>
    private ${column.attrType} ${column.attrName};
    </#if>
</#list>
}
