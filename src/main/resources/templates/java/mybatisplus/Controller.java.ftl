package ${basePackage}.${controller};

import com.github.pagehelper.PageInfo;
<#if enableGenerateDoc>
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
</#if>
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.validation.annotation.Validated;
import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Collections;
<#if haveBigDecimal>
import java.math.BigDecimal;
</#if>
<#if haveLocalDate>
import java.time.LocalDate;
</#if>
<#if haveLocalDateTime>
import java.time.LocalDateTime;
</#if>
<#if haveLocalTime>
import java.time.LocalTime;
</#if>
import ${basePackage}.vo.${className}VO;
import ${basePackage}.pojo.Add${className}POJO;
import ${basePackage}.pojo.Update${className}POJO;
import ${basePackage}.${service}.impl.${className}ServiceImpl;
import javax.validation.Valid;

/**
 * ${comment}接口
 *
 * @author ${author}
 * @since ${createTime}
 */
<#if enableGenerateDoc>
@Api(tags = "${comment}接口")
</#if>
@Validated
@RestController
@RequestMapping(value = "${apiPrefix}/${className?uncap_first}")
@RequiredArgsConstructor(onConstructor_ = @Autowired)
public class ${className}Controller extends BaseController {

    private final ${className}ServiceImpl ${className?uncap_first}Service;

    /**
	 * 获取列表(分页)
	 *
	 * @param pageNum     分页参数
	 * @param pageSize    分页参数
     <#list columns as column>
         <#if (column.attrName == "createBy" || column.attrName == "createUser" || column.attrName == "updateBy" || column.attrName == "updateUser" || column.attrName == "createAt" || column.attrName == "createTime" || column.attrName == "createDate" || column.attrName == "updateAt" || column.attrName == "updateTime" || column.attrName == "updateDate" || column.attrName == "valid" || column.attrName == "deleted")>
         <#else>
     * @param ${column.attrName} ${column.columnComment}
         </#if>
     </#list>
	 * @return {@link ResponseEntity${r'<List<'}${className}VO>>}
	 **/
    <#if enableGenerateDoc>
    @ApiOperation(value = "获取列表(分页)", notes = "获取列表(分页)")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "pageNum", value = "分页参数", dataType = "java.lang.Integer", example = "1", required = true),
            @ApiImplicitParam(name = "pageSize", value = "分页参数(取值范围：1~50)", dataType = "java.lang.Integer", example = "10", required = true),
            <#list columns as column>
            <#if (column.attrName == "createBy" || column.attrName == "createUser" || column.attrName == "updateBy" || column.attrName == "updateUser" || column.attrName == "createAt" || column.attrName == "createTime" || column.attrName == "createDate" || column.attrName == "updateAt" || column.attrName == "updateTime" || column.attrName == "updateDate" || column.attrName == "valid" || column.attrName == "deleted")>
            <#else>
            @ApiImplicitParam(name = "${column.attrName}", value = "${column.columnComment}", dataType = "${column.fullAttrType}", example = "${column.example}", required = false)<#if column_has_next>,</#if>
            </#if>
            </#list>
    })
    </#if>
    @GetMapping(value = "/getList")
    public ResponseEntity${r'<List<'}${className}VO>> getList(
            @RequestParam(value = "pageNum", required = true) Integer pageNum,
            @RequestParam(value = "pageSize", required = true) Integer pageSize,
            <#list columns as column>
            <#if (column.attrName == "createBy" || column.attrName =="createUser" || column.attrName == "updateBy" || column.attrName == "updateUser" || column.attrName == "createAt" || column.attrName == "createTime" || column.attrName == "createDate" || column.attrName == "updateAt" || column.attrName == "updateTime" || column.attrName == "updateDate" || column.attrName == "valid" || column.attrName == "deleted")>
            <#else>
            @RequestParam(value = "${column.attrName}", required = false) ${column.attrType} ${column.attrName}<#if column_has_next>,</#if>
            </#if>
            </#list>
    ) {

        ${className}VO param = new ${className}VO();
        <#list columns as column>
        <#if (column.attrName == "createBy" || column.attrName =="createUser" || column.attrName == "updateBy" || column.attrName == "updateUser" || column.attrName == "createAt" || column.attrName == "createTime" || column.attrName == "createDate" || column.attrName == "updateAt" || column.attrName == "updateTime" || column.attrName == "updateDate" || column.attrName == "valid" || column.attrName == "deleted")>
        <#else>
        param.set${column.upcaseAttrName}(${column.attrName});
        </#if>
        </#list>

        PageInfo${r'<'}${className}VO> pageInfo = ${className?uncap_first}Service.pageList(pageNum, pageSize, param);
        if(pageInfo == null) {
            return ResponseEntity.success(Collections.emptyList(), 0L);
        } else {
            return ResponseEntity.success(pageInfo.getList(), pageInfo.getTotal());
        }
    }

    /**
     * 查询一条数据
     *
     <#list pkColumns as column>
     * @param ${column.attrName} ${column.columnComment}
     </#list>
     * @return {@link ResponseEntity${r'<'}${className}VO>}
     */
    <#if enableGenerateDoc>
    @ApiOperation(value = "查询一条数据", notes = "查询一条数据")
    @ApiImplicitParams({
        <#list pkColumns as column>
        @ApiImplicitParam(name = "${column.attrName}", value = "${column.columnComment}", dataType = "${column.fullAttrType}", example = "123456", required = true)<#if column_has_next>,</#if>
        </#list>
    })
    </#if>
    @GetMapping(value = "/getOne")
    public ResponseEntity${r'<'}${className}VO> getOne(<#list pkColumns as column>@RequestParam(value = "${column.attrName}") ${column.attrType} ${column.attrName}<#if column_has_next>, </#if></#list>) {
        ${className}VO result = ${className?uncap_first}Service.getOneByPk(<#list pkColumns as column>${column.attrName}<#if column_has_next>, </#if></#list>);

        return ResponseEntity.success(result);
    }

    /**
     * 新增数据
     *
     * @param data    待新增数据
     * @return {@link ResponseEntity${r'<'}String>}
     **/
    <#if enableGenerateDoc>
    @ApiOperation(value = "新增数据", notes = "新增数据")
    </#if>
    @PostMapping(value = "/add")
    public ResponseEntity${r'<'}String> add(@Valid @RequestBody Add${className}POJO data) {
        boolean result = ${className?uncap_first}Service.add(data);

        return result ? ResponseEntity.success() : ResponseEntity.failure();
    }

    /**
     * 批量新增
     *
     * @param datas    待新增数据集
     * @return {@link ResponseEntity${r'<'}String>}
     **/
    <#if enableGenerateDoc>
    @ApiOperation(value = "批量新增", notes = "批量新增")
    </#if>
    @PostMapping(value = "/addBatch")
    public ResponseEntity${r'<'}String> addBatch(@Valid @RequestBody List${r'<'}Add${className}POJO> datas) {
        boolean result = ${className?uncap_first}Service.addBatch(datas);

        return result ? ResponseEntity.success() : ResponseEntity.failure();
    }

    /**
     * 更新数据
     *
     * @param data     更新数据
     * @return {@link ResponseEntity${r'<'}String>}
     **/
    <#if enableGenerateDoc>
    @ApiOperation(value = "更新数据", notes = "更新数据")
    </#if>
    @PutMapping(value = "/modify")
    public ResponseEntity${r'<'}String> modify(@Valid @RequestBody Update${className}POJO data) {\
        boolean result = ${className?uncap_first}Service.modify(data);

        return result ? ResponseEntity.success() : ResponseEntity.failure();
    }

   /**
    * 删除一条数据
    *
    <#list pkColumns as column>
    * @param ${column.attrName} ${column.columnComment}
    </#list>
    * @return {@link ResponseEntity${r'<'}String>}
    **/
    <#if enableGenerateDoc>
    @ApiOperation(value = "删除一条数据", notes = "删除一条数据")
    @ApiImplicitParams({
        <#list pkColumns as column>
        @ApiImplicitParam(name = "${column.attrName}", value = "${column.columnComment}", dataType = "${column.fullAttrType}", example = "123456", required = true)<#if column_has_next>,</#if>
        </#list>
    })
    </#if>
    @DeleteMapping(value = "/delete")
    public ResponseEntity${r'<'}String> delete(<#list pkColumns as column>@RequestParam(value = "${column.attrName}") ${column.attrType} ${column.attrName}<#if column_has_next>,</#if></#list>) {
        boolean result = ${className?uncap_first}Service.removeOne(<#list pkColumns as column>${column.attrName}<#if column_has_next>, </#if></#list>);

        return result ? ResponseEntity.success() : ResponseEntity.failure();
    }

}