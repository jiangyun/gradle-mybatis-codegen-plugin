${r'<?xml version="1.0" encoding="UTF-8" ?>'}
${r'<!DOCTYPE mapper PUBLIC "-//mybatis.org//DTD Mapper 3.0//EN" "http://mybatis.org/dtd/mybatis-3-mapper.dtd" >'}

${r'<'}mapper namespace="${basePackage}.${mapper}.${className}Mapper">
    <!-- 结果映射关系 -->
    ${r'<'}resultMap id="AllColumnMap" type="${basePackage}.vo.${className}VO">
<#list columns as column>
        ${r'<'}result column="${column.columnName}" property="${column.attrName}"/>
</#list>
    ${r'<'}/resultMap>

    <!-- 表字段的sql -->
    ${r'<'}sql id="all_column">
    <#list columns as column>
        ${column.columnName}<#if column_has_next>,</#if>
    </#list>
    ${r'<'}/sql>

    <!-- 查询列表 -->
    ${r'<'}select id="select" resultType="${basePackage}.vo.${className}VO">
        SELECT
        <#list columns as column>
            t1.${column.columnName} AS ${column.attrName}<#if column_has_next>,</#if>
        </#list>
        FROM ${tableName} t1
        ${r'<'}where>
        <#list columns as column>
            ${r'<'}if test="pojo.${column.attrName} != null"> AND t1.${column.columnName} = ${r'#'}{pojo.${column.attrName}}${r'<'}/if>
        </#list>
        ${r'<'}/where>
    ${r'<'}/select>
    <!-- 此处统计总条数为上面的方法附属查询方法，查询条件与上面的务必要一致 -->
    ${r'<'}select id="select_COUNT" resultType="java.lang.Long">
        SELECT
        COUNT(*)
        FROM ${tableName} t1
        ${r'<'}where>
        <#list columns as column>
            ${r'<'}if test="pojo.${column.attrName} != null"> AND t1.${column.columnName} = ${r'#'}{pojo.${column.attrName}}${r'<'}/if>
        </#list>
        ${r'<'}/where>
    ${r'<'}/select>

    <!-- 查询列表(仅查询单表字段) -->
    ${r'<'}select id="selectOnlySelf" resultMap="AllColumnMap">
        SELECT
        <#list columns as column>
            ${column.columnName}<#if column_has_next>,</#if>
        </#list>
        FROM ${tableName}
        ${r'<'}where>
        <#list columns as column>
            ${r'<'}if test="pojo.${column.attrName} != null"> AND ${column.columnName} = ${r'#'}{pojo.${column.attrName}}${r'<'}/if>
        </#list>
        ${r'<'}/where>
    ${r'<'}/select>

    <!-- 根据主键查询一条记录 -->
    ${r'<'}select id="selectById" resultType="${basePackage}.vo.${className}VO">
        SELECT
        <#list columns as column>
            t1.${column.columnName} AS ${column.attrName}<#if column_has_next>,</#if>
        </#list>
        FROM ${tableName} t1
        ${r'<'}where>
        <#list pkColumns as column>
            <#if (column_index > 0)>AND </#if>${column.columnName} = ${r'#'}{pojo.${column.attrName}}
            <#else>
        </#list>
        ${r'<'}/where>
    ${r'<'}/select>

    <!-- 新增数据 -->
    ${r'<'}insert id="insert">
        INSERT INTO ${tableName}
        ${r'<'}trim prefix="(" suffix=")" suffixOverrides=",">
        <#list columns as column>
            ${r'<if test="pojo.'}${column.attrName} != null"> ${column.columnName}, ${r'</if>'}
        </#list>
        ${r'<'}/trim>
        VALUES
        ${r'<'}trim prefix="(" suffix=")" suffixOverrides=",">
        <#list columns as column>
            ${r'<if test="pojo.'}${column.attrName} != null"> ${r'#'}{pojo.${column.attrName}}, ${r'</if>'}
        </#list>
        ${r'<'}/trim>
    ${r'<'}/insert>

    <!-- 批量新增数据 -->
    ${r'<'}insert id="insertBatch">
        INSERT INTO ${tableName}
        (${r'<'}include refid="all_column"/>)
        VALUES
        ${r'<'}foreach collection="pojos" item="pojo" index="index" separator=",">
        (
        <#list columns as column>
            ${r'<if test="pojo.'}${column.attrName} != null"> ${r'#'}{pojo.${column.attrName}}, ${r'</if>'}
        </#list>
        )
        ${r'<'}/foreach>
    ${r'<'}/insert>

    <!-- 更新数据 -->
    ${r'<'}update id="update">
        UPDATE ${tableName}
        ${r'<'}set>
        <#list columns as column>
            ${r'<if test="pojo.'}${column.attrName} != null">${column.columnName} = ${r'#'}{pojo.${column.attrName}}<#if column_has_next>, </#if>${r'</if>'}
        </#list>
        ${r'<'}/set>
        WHERE
        <#list pkColumns as column>
            <#if (column_index > 0)>AND </#if>${column.columnName} = ${r'#'}{pojo.${column.attrName}}
        </#list>
    ${r'<'}/update>

    <!-- 逻辑删除 -->
    ${r'<'}update id="delete">
        UPDATE ${tableName}
        SET valid = 0, update_by = ${r'#'}{actionUserId}, update_at = NOW()
        WHERE
        <#list pkColumns as column>
            <#if (column_index > 0)>AND </#if>${column.columnName} = ${r'#'}{pojo.${column.attrName}}
        </#list>
    ${r'<'}/update>

    <!-- 删除数据-物理删除(使用该方法务必看业务是否切实需求) -->
    ${r'<'}delete id="deleteByPk">
		DELETE
        FROM ${tableName}
        WHERE
        <#list pkColumns as column>
             <#if (column_index > 0)>AND </#if>${column.columnName} = ${r'#'}{${column.attrName}}
        </#list>
    ${r'<'}/delete>

${r'<'}/mapper>