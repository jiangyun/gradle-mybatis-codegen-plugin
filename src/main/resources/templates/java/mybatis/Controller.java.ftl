package ${basePackage}.${controller};

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
<#if enableGenerateDoc>
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
</#if>
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Collections;
<#if haveBigDecimal>
import java.math.BigDecimal;
</#if>
<#if haveDate>
import java.util.Date;
</#if>
import javax.validation.Valid;
import ${basePackage}.vo.${className}VO;
import ${basePackage}.pojo.${className}POJO;
import ${basePackage}.common.ResponseEntity;
import ${basePackage}.${service}.${className}Service;

/**
 * @描述: ${comment}
 * @作者: ${author}
 * @创建时间: ${createTime}
 * @所属公司: ${company}
 */
<#if enableGenerateDoc>
@Api(tags = "${comment}接口", description = "${comment}接口")
</#if>
@RestController
@RequestMapping("${apiPrefix}/${className?uncap_first}")
public class ${className}Controller {

    @Autowired
    private ${className}Service m${className}Service;

    /**
	 * 查询列表
	 *
	 * @param pageNum     分页参数
	 * @param pageSize    分页参数
     <#list columns as column>
     * @param ${column.attrName} ${column.columnComment}
     </#list>
     * @param request
	 * @return
	 **/
    <#if enableGenerateDoc>
    @ApiOperation(value = "获取列表(分页)", notes = "获取列表(分页)")
    @ApiOperationSupport(order = 1)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "pageNum", value = "分页参数", dataType = "java.lang.Integer", example = "1", required = true),
            @ApiImplicitParam(name = "pageSize", value = "分页参数(取值范围：1~50)", dataType = "java.lang.Integer", example = "10", required = true),
            <#list columns as column>
            @ApiImplicitParam(name = "${column.attrName}", value = "${column.columnComment}", dataType = "${column.fullAttrType}", example = "${column.example}", required = false)<#if column_has_next>,</#if>
            </#list>
    })
    </#if>
    @GetMapping(value = "/getList")
    public ResponseEntity${r'<List<'}${className}VO>> getList(
            @RequestParam(value = "pageNum", required = true) Integer pageNum,
            @RequestParam(value = "pageSize", required = true) Integer pageSize,
            <#list columns as column>
            @RequestParam(value = "${column.attrName}", required = false) ${column.attrType} ${column.attrName},
            </#list>
            HttpServletRequest request) {

        pageNum = pageNum < 0 ? 1 : pageNum;
        pageSize = pageSize < 0 || pageSize > 50 ? 10 : pageSize;
        ${className}VO param = new ${className}VO();
        <#list columns as column>
        param.set${column.upcaseAttrName}(${column.attrName});
        </#list>

        PageHelper.startPage(pageNum, pageSize);
        List${r'<'}${className}VO> results = m${className}Service.getList(param);
        if(results == null) {
            return ResponseEntity.success(Collections.EMPTY_LIST, 0L);
        } else {
            PageInfo${r'<'}${className}VO> pageInfo = new PageInfo<>(results);
            return ResponseEntity.success(pageInfo.getList(), pageInfo.getTotal());
        }
    }

    /**
     * 根据ID查询数据
     *
     <#list pkColumns as column>
     * @param ${column.attrName} ${column.columnComment}
     </#list>
     * @return
     */
    <#if enableGenerateDoc>
    @ApiOperation(value = "根据ID查询数据", notes = "根据ID查询数据")
    @ApiOperationSupport(order = 2)
    @ApiImplicitParams({
        <#list pkColumns as column>
        @ApiImplicitParam(name = "${column.attrName}", value = "${column.columnComment}", dataType = "${column.fullAttrType}", example = "f64ba455b9a8496a9007b50f39716253", required = true)<#if column_has_next>,</#if>
        </#list>
    })
    </#if>
    @GetMapping(value = "/getOneById")
    public ResponseEntity${r'<'}${className}VO> getOneById(<#list pkColumns as column>@RequestParam(value = "${column.attrName}") ${column.attrType} ${column.attrName}<#if column_has_next>, </#if></#list>) {
        ${className}VO result = m${className}Service.getById(<#list pkColumns as column>${column.attrName}<#if column_has_next>, </#if></#list>);

        return ResponseEntity.success(result);
    }

    /**
     * 新增数据
     *
     * @param param    待新增数据
     * @param request
     * @return
     **/
    <#if enableGenerateDoc>
    @ApiOperation(value = "新增数据", notes = "新增数据")
    @ApiOperationSupport(order = 3)
    </#if>
    @PostMapping(value = "/add")
    public ResponseEntity${r'<'}String> add(@RequestBody @Valid ${className}POJO param, HttpServletRequest request) {
<#list columns as column>
    <#if (column.required && column.columnKey != "PRI" && column.attrName != "valid" && column.attrName != "createBy" && column.attrName != "updateBy" && column.attrName != "createAt" && column.attrName != "updateAt" && column.attrName != "createTime" && column.attrName != "updateTime")>
        <#if (column.attrType == "String")>
        if (StringUtils.isBlank(data.get${column.upcaseAttrName}())) {
            return ResponseEntity.error("${column.attrName}不能为空");
        }
        <#else>
        if (data.get${column.upcaseAttrName}() == null) {
            return ResponseEntity.error("${column.attrName}不能为空");
        }
        </#if>
    </#if>
</#list>
        int result = m${className}Service.add(param, JwtUtils.getUserIdFromRequest(request));
        if (result < 1) {
            return ResponseEntity.failure();
        } else {
            return ResponseEntity.success();
        }
    }

    /**
     * 批量新增
     *
     * @param params    待新增数据
     * @param request
     * @return
     **/
    <#if enableGenerateDoc>
    @ApiOperation(value = "批量新增", notes = "批量新增")
    @ApiOperationSupport(order = 4)
    </#if>
    @PostMapping(value = "/addBatch")
    public ResponseEntity${r'<'}String> addBatch(@RequestBody @Valid List${r'<'}${className}POJO> params, HttpServletRequest request) {
        for (${className}POJO data : datas) {
<#list columns as column>
    <#if (column.required && column.columnKey != "PRI" && column.attrName != "valid" && column.attrName != "createBy" && column.attrName != "updateBy" && column.attrName != "createAt" && column.attrName != "updateAt" && column.attrName != "createTime" && column.attrName != "updateTime")>
        <#if (column.attrType == "String")>
            if (StringUtils.isBlank(data.get${column.upcaseAttrName}())) {
                return ResponseEntity.error("${column.attrName}不能为空");
            }
        <#else>
            if (data.get${column.upcaseAttrName}() == null) {
                return ResponseEntity.error("${column.attrName}不能为空");
            }
        </#if>
    </#if>
</#list>
        }
        int result = m${className}Service.addBatch(params, JwtUtils.getUserIdFromRequest(request));
        if (result < 1) {
            return ResponseEntity.failure();
        } else {
            return ResponseEntity.success();
        }
    }

    /**
     * 更新数据
     *
     * @param param     更新数据
     * @param request
     * @return
     **/
    <#if enableGenerateDoc>
    @ApiOperation(value = "更新数据", notes = "更新数据")
    @ApiOperationSupport(order = 5)
    </#if>
    @PutMapping(value = "/modify")
    public ResponseEntity${r'<'}String> modify(@RequestBody @Valid ${className}POJO param, HttpServletRequest request) {
    <#list pkColumns as column>
        if (StringUtils.isBlank(param.get${column.upcaseAttrName}())) {
            return ResponseEntity.failure("ID不能为空");
        }
    </#list>
        int result = m${className}Service.modify(param, JwtUtils.getUserIdFromRequest(request));
        if (result < 1) {
            return ResponseEntity.failure();
        } else {
            return ResponseEntity.success();
        }
    }

   /**
    * 逻辑删除
    *
    <#list pkColumns as column>
    * @param ${column.attrName} ${column.columnComment}
    </#list>
    * @param request
    * @return
    **/
    <#if enableGenerateDoc>
    @ApiOperation(value = "逻辑删除", notes = "逻辑删除")
    @ApiOperationSupport(order = 6)
    @ApiImplicitParams({
        <#list pkColumns as column>
        @ApiImplicitParam(name = "${column.attrName}", value = "${column.columnComment}", dataType = "${column.fullAttrType}", example = "f64ba455b9a8496a9007b50f39716253", required = true)<#if column_has_next>,</#if>
        </#list>
    })
    </#if>
    @DeleteMapping(value = "/delete")
    public ResponseEntity${r'<'}String> delete(<#list pkColumns as column>@RequestParam(value = "${column.attrName}") ${column.attrType} ${column.attrName}, </#list>HttpServletRequest request) {
        int result = m${className}Service.delete(<#list pkColumns as column>${column.attrName}, </#list> JwtUtils.getUserIdFromRequest(request));
        if (result < 1) {
            return ResponseEntity.failure();
        } else {
            return ResponseEntity.success();
        }
    }

}