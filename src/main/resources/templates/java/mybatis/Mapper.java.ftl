package ${basePackage}.${mapper};

import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;
<#if haveBigDecimal>
import java.math.BigDecimal;
</#if>
<#if haveDate>
import java.util.Date;
</#if>
import java.util.List;

import ${basePackage}.${entity}.${className}Entity;
import ${basePackage}.vo.${className}VO;

/**
 * @描述: ${comment}
 * @作者: ${author}
 * @创建时间: ${createTime}
 * @所属公司: ${company}
 */
@Repository
public interface ${className}Mapper {

    /**
	 * 查询列表
	 *
	 * @param pojo 查询过滤参数
	 * @return
	 **/
    List${r'<'}${className}VO> select(@Param(value = "pojo") ${className}VO pojo);

    /**
     * 查询列表(仅查询单表字段)
     *
     * @param pojo 查询过滤参数
     * @return
     **/
    List${r'<'}${className}VO> selectOnlySelf(@Param(value = "pojo") ${className}VO pojo);

    /**
     * 根据ID查询数据
     *
     <#list pkColumns as column>
     * @param ${column.attrName} ${column.columnComment}
     </#list>
     * @return
     **/
    ${className}VO selectById(<#list pkColumns as column>@Param(value = "${column.attrName}") ${column.attrType} ${column.attrName}<#if column_has_next>, </#if></#list>);

    /**
     * 新增数据
     *
     * @param pojo 待新增数据
     * @return
     **/
    int insert(@Param(value = "pojo") ${className}Entity pojo);

    /**
     * 批量新增
     *
     * @param pojos 待新增数据集合
     * @return
     **/
    int insertBatch(@Param(value = "pojos") List${r'<'}${className}Entity> pojos);

    /**
     * 更新数据
     *
     * @param pojo 待更新数据
     * @return
     **/
    int update(@Param(value = "pojo") ${className}Entity pojo);

    /**
     * 逻辑删除
     *
     <#list pkColumns as column>
     * @param ${column.attrName} ${column.columnComment}
     </#list>
     * @param actionUserId 操作用户ID
     * @return
     **/
    int delete(<#list pkColumns as column>@Param(value = "${column.attrName}") ${column.attrType} ${column.attrName}, </#list> @Param(value = "actionUserId") String actionUserId);

    /**
     * 物理删除
     *
     <#list pkColumns as column>
     * @param ${column.attrName} ${column.columnComment}
     </#list>
     * @return
     **/
     int deleteByPk(<#list pkColumns as column>@Param(value = "${column.attrName}") ${column.attrType} ${column.attrName}<#if column_has_next>, </#if></#list>);

}