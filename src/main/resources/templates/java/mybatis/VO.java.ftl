package ${basePackage}.vo;

<#if enableGenerateDoc>
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
</#if>
import lombok.Getter;
import lombok.Setter;
import java.io.Serializable;
<#if haveBigDecimal>
import java.math.BigDecimal;
</#if>
<#if haveDate>
import java.util.Date;
</#if>

/**
 * @描述: ${comment}
 * @作者: ${author}
 * @创建时间: ${createTime}
 * @所属公司: ${company}
 */
<#if enableGenerateDoc>
@ApiModel(value = "${comment}信息")
</#if>
@Getter
@Setter
public class ${className}VO{
<#list columns as column>
    /**
     * ${column.columnComment}
     */
    <#if enableGenerateDoc>
    @ApiModelProperty(value = "${column.columnComment}<#if column.characterMaximumLength??>【允许最大长度：${column.characterMaximumLength}】</#if>", dataType = "${column.fullAttrType}", example = "${column.example}")
    </#if>
    private ${column.attrType} ${column.attrName};

</#list>
}
