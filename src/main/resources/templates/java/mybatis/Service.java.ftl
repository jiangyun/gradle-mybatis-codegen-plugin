package ${basePackage}.${service};

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.ArrayList;
<#if haveBigDecimal>
import java.math.BigDecimal;
</#if>
<#if haveDate>
import java.util.Date;
</#if>

import ${basePackage}.${entity}.${className}Entity;
import ${basePackage}.vo.${className}VO;
import ${basePackage}.pojo.${className}POJO;
import ${basePackage}.${mapper}.${className}Mapper;
import ${basePackage}.utils.GenerateCodeUtils;

/**
 * @描述: ${comment}
 * @作者: ${author}
 * @创建时间: ${createTime}
 * @所属公司: ${company}
 */
@Service
public class ${className}Service {
    @Autowired
    private ${className}Mapper m${className}Mapper;

    /**
      * 查询列表
      *
      * @param param 查询过滤参数
      * @return
      **/
     public List${r'<'}${className}VO> getList(${className}VO param) {
         List${r'<'}${className}VO> results = m${className}Mapper.select(param);

         return results;
     }

     /**
      * 查询列表(仅查询单表字段)
      *
      * @param param 查询过滤参数
      * @return
      **/
      public List${r'<'}${className}VO> getOnlySelf(${className}VO param) {
          List${r'<'}${className}VO> results = m${className}Mapper.selectOnlySelf(param);

          return results;
      }

     /**
      * 根据ID查询数据
      *
      <#list pkColumns as column>
      * @param ${column.attrName} ${column.columnComment}
      </#list>
      * @return
      **/
     public ${className}VO getById(<#list pkColumns as column>${column.attrType} ${column.attrName}<#if column_has_next>, </#if></#list>) {
         ${className}VO result = m${className}Mapper.selectById(<#list pkColumns as column>${column.attrName}<#if column_has_next>, </#if></#list>);

         return result;
     }

     /**
      * 新增数据
      *
      * @param data         待新增数据
      * @param actionUserId  操作用户ID
      * @return
      **/
     public int add(${className}POJO data, String actionUserId) {
        ${className}Entity param = new ${className}Entity();
         <#list columns as column>
            <#if (pkColumns?size == 1 && column.columnKey == "PRI")>
         param.set${column.upcaseAttrName}(GenerateCodeUtils.generateId());
            <#elseif (column.attrName == "createBy" || column.attrName =="createUser" || column.attrName == "updateBy" || column.attrName == "updateUser")>
         param.set${column.upcaseAttrName}(actionUserId);
            <#elseif (column.attrName == "createAt" || column.attrName == "createTime" || column.attrName == "createDate" || column.attrName == "updateAt" || column.attrName == "updateTime" || column.attrName == "updateDate")>
         param.set${column.upcaseAttrName}(new Date());
            <#elseif (column.attrName == "valid")>
         param.set${column.upcaseAttrName}(1);
            <#else>
         param.set${column.upcaseAttrName}(data.get${column.upcaseAttrName}());
            </#if>
         </#list>

         int result = m${className}Mapper.insert(param);

         return result;
     }

     /**
      * 批量新增
      *
      * @param datas        待新增数据集合
      * @param actionUserId  操作用户ID
      * @return
      **/
     public int addBatch(List${r'<'}${className}POJO> datas, String actionUserId) {
         List${r'<'}${className}Entity> params = new ArrayList<>();
         ${className}Entity param;
         for(${className}POJO data : datas) {
            param = new ${className}Entity();
            <#list columns as column>
                <#if (pkColumns?size == 1 && column.columnKey == "PRI")>
            param.set${column.upcaseAttrName}(GenerateCodeUtils.generateId());
                <#elseif (column.attrName == "createBy" || column.attrName =="createUser" || column.attrName == "updateBy" || column.attrName == "updateUser")>
            param.set${column.upcaseAttrName}(actionUserId);
                <#elseif (column.attrName == "createAt" || column.attrName == "createTime" || column.attrName == "createDate" || column.attrName == "updateAt" || column.attrName == "updateTime" || column.attrName == "updateDate")>
            param.set${column.upcaseAttrName}(new Date());
                <#elseif (column.attrName == "valid")>
            param.set${column.upcaseAttrName}(1);
                <#else>
            param.set${column.upcaseAttrName}(data.get${column.upcaseAttrName}());
                </#if>
            </#list>

            params.add(param);
         }
         int result = m${className}Mapper.insertBatch(params);

         return result;
     }

     /**
      * 更新数据
      *
      * @param data          待更新数据
      * @param actionUserId  操作用户ID
      * @return
      **/
     public int modify(${className}POJO data, String actionUserId) {
         ${className}Entity param = new ${className}Entity();
         <#list columns as column>
            <#if (column.attrName == "updateBy" || column.attrName == "updateUser")>
         param.set${column.upcaseAttrName}(actionUserId);
            <#elseif (column.attrName == "updateAt" || column.attrName == "updateTime" || column.attrName == "updateDate")>
         param.set${column.upcaseAttrName}(new Date());
            <#elseif (column.attrName == "valid")>
         param.set${column.upcaseAttrName}(1);
            <#else>
         param.set${column.upcaseAttrName}(data.get${column.upcaseAttrName}());
            </#if>
         </#list>

         int result = m${className}Mapper.update(param);

         return result;
     }

     /**
      * 逻辑删除
      *
      <#list pkColumns as column>
      * @param ${column.attrName} ${column.columnComment}
      </#list>
      * @param actionUserId 操作用户ID
      * @return
      **/
     public int delete(<#list pkColumns as column>${column.attrType} ${column.attrName}, </#list>String actionUserId) {
         int result = m${className}Mapper.delete(<#list pkColumns as column>${column.attrName}, </#list> actionUserId);

         return result;
     }
}