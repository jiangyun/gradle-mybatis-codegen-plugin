package ${basePackage}.common;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang3.StringUtils;

<#if enableGenerateDoc>
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
</#if>

/**
 * @描述: 统一响应结果
 * @作者: ${author}
 * @创建时间: ${createTime}
 * @所属公司: ${company}
 */
<#if enableGenerateDoc>
@ApiModel(value = "统一响应结果")
</#if>
@Getter
@Setter
@AllArgsConstructor
public class ResponseEntity${r'<T>'} {

    public static final String CODE_SUCCESS = "000000";
    public static final String CODE_FAILURE = "100000";
    // 参数错误错误码
    public static final String PARAM_ERROR_CODE = "100001";
    public static final String CODE_ERROR = "200000";

    public static final String MSG_SUCCESS = "成功";
    public static final String MSG_FAILURE = "失败";
    public static final String MSG_ERROR = "错误";

    /**
     * 返回信息
     */
    <#if enableGenerateDoc>
    @ApiModelProperty(value = "返回信息", dataType = "string", example = "成功")
    </#if>
    private String message;

    /**
     * 返回码
     */
    <#if enableGenerateDoc>
    @ApiModelProperty(value = "返回码", dataType = "string", example = "000000")
    </#if>
    private String code;

    /**
     * 返回的数据
     */
    <#if enableGenerateDoc>
    @ApiModelProperty(value = "返回的数据")
    </#if>
    private T data;

    /**
     * 数据长度
     */
    <#if enableGenerateDoc>
    @ApiModelProperty(value = "数据长度", dataType = "int", example = "1000")
    </#if>
    private Long total;

    public static ${r'<T>'} ResponseEntity success() {
        return new ResponseEntity(MSG_SUCCESS, CODE_SUCCESS, null, null);
    }

    public static ${r'<T>'} ResponseEntity success(String msg) {
        return new ResponseEntity(!StringUtils.isEmpty(msg) ? msg : MSG_SUCCESS, CODE_SUCCESS, null, null);
    }

    public static ${r'<T>'} ResponseEntity success(T data, String msg) {
        return new ResponseEntity(!StringUtils.isEmpty(msg) ? msg : MSG_SUCCESS, CODE_SUCCESS, data, null);
    }

    public static ${r'<T>'} ResponseEntity success(T data) {
        return new ResponseEntity(MSG_SUCCESS, CODE_SUCCESS, data, null);
    }

    public static ${r'<T>'} ResponseEntity success(T data, Long total) {
        return new ResponseEntity(MSG_SUCCESS, CODE_SUCCESS, data, total);
    }

    public static ${r'<T>'} ResponseEntity success(T data, String msg, Long total) {
        return new ResponseEntity(!StringUtils.isEmpty(msg) ? msg : MSG_SUCCESS, CODE_SUCCESS, data, total);
    }

    public static ${r'<T>'} ResponseEntity failure() {
        return new ResponseEntity(MSG_FAILURE, CODE_FAILURE, null, null);
    }

    public static ${r'<T>'} ResponseEntity failure(String msg) {
        return new ResponseEntity(!StringUtils.isEmpty(msg) ? msg : MSG_FAILURE, CODE_FAILURE, null, null);
    }

    public static ${r'<T>'} ResponseEntity failure(String code,String msg) {
        return new ResponseEntity(!StringUtils.isEmpty(msg) ? msg : MSG_FAILURE, code, null, null);
    }

    public static ${r'<T>'} ResponseEntity error() {
        return new ResponseEntity(MSG_ERROR, CODE_ERROR, null, null);
    }

    public static ${r'<T>'} ResponseEntity error(String msg) {
        return new ResponseEntity(!StringUtils.isEmpty(msg) ? msg : MSG_ERROR, CODE_ERROR, null, null);
    }

    public static ${r'<T>'} ResponseEntity error(String code, String msg) {
        return new ResponseEntity(msg, code, null, null);

    }public static ${r'<T>'} ResponseEntity error(String code,T data ) {
        return new ResponseEntity(null ,code,data, null);
    }
}
