package ${basePackage}.pojo;

<#if enableGenerateDoc>
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
</#if>
import lombok.Getter;
import lombok.Setter;
import java.io.Serializable;
<#if haveBigDecimal>
import java.math.BigDecimal;
</#if>
<#if haveDate>
import java.util.Date;
</#if>

/**
 * @描述: ${comment}
 * @作者: ${author}
 * @创建时间: ${createTime}
 * @所属公司: ${company}
 */
<#if enableGenerateDoc>
@ApiModel(value = "${comment}-新增/更新")
</#if>
@Getter
@Setter
public class ${className}DTO {
<#list columns as column>
    /**
     * ${column.columnComment}
     */
    <#if enableGenerateDoc>
    @ApiModelProperty(value = "${column.columnComment}", dataType = "${column.fullAttrType}", example = "${column.example}", required = ${column.required?string("true","false")})
    </#if>
    private ${column.attrType} ${column.attrName};

</#list>
}
