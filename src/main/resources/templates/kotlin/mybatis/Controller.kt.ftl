package ${basePackage}.${controller}

import com.github.pagehelper.PageInfo
<#if enableGenerateDoc>
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport
import io.swagger.annotations.Api
import io.swagger.annotations.ApiImplicitParam
import io.swagger.annotations.ApiImplicitParams
import io.swagger.annotations.ApiOperation
</#if>
import org.springframework.web.bind.annotation.*
import org.springframework.validation.annotation.Validated;
import javax.servlet.http.HttpServletRequest

import ${basePackage}.vo.${className}VO
import ${basePackage}.pojo.${className}POJO
import ${basePackage}.common.ResponseEntity
import ${basePackage}.${service}.${className}Service
import javax.validation.Valid;

/**
 * ${comment}接口
 * @date ${createTime}
 */
<#if enableGenerateDoc>
@Api(tags = "${comment}接口")
</#if>
@Validated
@RestController
@RequestMapping(value = ["/${apiPrefix}/${className?uncap_first}"])
class ${className}Api(val ${className?uncap_first}Service: ${className}Service) {

    /**
	 * 查询列表
	 *
	 * @param pageNum     分页参数
	 * @param pageSize    分页参数
     <#list columns as column>
         <#if (column.attrName == "createBy" || column.attrName == "createUser" || column.attrName == "updateBy" || column.attrName == "updateUser" || column.attrName == "createAt" || column.attrName == "createTime" || column.attrName == "createDate" || column.attrName == "updateAt" || column.attrName == "updateTime" || column.attrName == "updateDate" || column.attrName == "valid" || column.attrName == "deleted")>
         <#else>
      * @param ${column.attrName} ${column.columnComment}
         </#if>
     </#list>
      * @return {@link ResponseEntity${r'<List<'}${className}VO>>}
      **/
    <#if enableGenerateDoc>
    @ApiOperation(value = "获取列表(分页)", notes = "获取列表(分页)")
    @ApiOperationSupport(order = 1)
    @ApiImplicitParams({
        @ApiImplicitParam(name = "pageNum", value = "分页参数", dataType = "java.lang.Integer", example = "1", required = true),
        @ApiImplicitParam(name = "pageSize", value = "分页参数(取值范围：1~50)", dataType = "java.lang.Integer", example = "10", required = true),
        <#list columns as column>
        @ApiImplicitParam(name = "${column.attrName}", value = "${column.columnComment}", dataType = "${column.fullAttrType}", example = "${column.example}", required = false)<#if column_has_next>,</#if>
        </#list>
    })
    </#if>
    @GetMapping(value = ["/getList"])
    fun getList(
        @RequestParam(value = "pageNum", required = false) pageNum: Int,
        @RequestParam(value = "pageSize", required = false) pageSize: Int,
        <#list columns as column>
        @RequestParam(value = "${column.attrName}", required = false) ${column.attrName}: ${column.attrType}?,
        </#list>
        request: HttpServletRequest
    ): ResponseEntity${r'<'}MutableList${r'<'}${className}VO>> {
        pageNum = if (pageNum < 0) 1 else pageNum
        pageSize = if (pageSize < 0 || pageSize > 50) 10 else pageSize

        val param: ${className}VO = ${className}VO()
        <#list columns as column>
        param.${column.attrName} = ${column.attrName}
        </#list>

        val pageInfo: PageInfo${r'<'}${className}VO>? = ${className?uncap_first}Service.getList(pageNum, pageSize, param)
        return if(pageInfo !== null) {
            ResponseEntity.success(data = pageInfo.list, total = pageInfo.total)
        } else {
            ResponseEntity.success(data = mutableListOf(), total = 0L)
        }
    }

    /**
     * 根据ID查询数据
     *
     <#list pkColumns as column>
     * @param ${column.attrName} ${column.columnComment}
     </#list>
     * @return
     */
    <#if enableGenerateDoc>
    @ApiOperation(value = "根据ID查询数据", notes = "根据ID查询数据")
    @ApiOperationSupport(order = 2)
    @ApiImplicitParams({
        <#list pkColumns as column>
        @ApiImplicitParam(name = "${column.attrName}", value = "${column.columnComment}", dataType = "${column.fullAttrType}", example = "f64ba455b9a8496a9007b50f39716253", required = true)<#if column_has_next>,</#if>
        </#list>
    })
    </#if>
    @GetMapping(value = ["/getOneById"])
    fun getOneById(<#list pkColumns as column>@RequestParam(value = "${column.attrName}") ${column.attrName}: ${column.attrType}<#if column_has_next>, </#if></#list>): ResponseEntity${r'<'}${className}VO> {
        val result: ${className}VO? = ${className?uncap_first}Service.getById(<#list pkColumns as column>${column.attrName}<#if column_has_next>, </#if></#list>)

        return ResponseEntity.success(result)
    }


    /**
     * 新增数据
     *
     * @param param    数据
     * @return
     **/
    <#if enableGenerateDoc>
    @ApiOperation(value = "新增数据", notes = "新增数据")
    @ApiOperationSupport(order = 3)
    </#if>
    @PostMapping(value = ["/add"])
    fun add(@Valid @RequestBody param: Add${className}POJO): ResponseEntity${r'<'}String> {
        val result: Boolean = ${className?uncap_first}Service.add(param)
        return if (result) {
             ResponseEntity.failure()
        } else {
             ResponseEntity.success()
        }
    }

    /**
     * 批量新增
     *
     * @param params    数据
     * @param request
     * @return
     **/
    <#if enableGenerateDoc>
    @ApiOperation(value = "批量新增", notes = "批量新增")
    @ApiOperationSupport(order = 4)
    </#if>
    @PostMapping(value = ["/addBatch"])
    fun addBatch(@Valid @RequestBody params: MutableList${r'<'}Add${className}POJO>): ResponseEntity${r'<'}String> {
        val result: Boolean = ${className?uncap_first}Service.addBatch(params)
        return if (result) {
            ResponseEntity.failure()
        } else {
            ResponseEntity.success()
        }
    }

    /**
     * 更新数据
     *
     * @param param     更新数据
     * @param request
     * @return
     **/
    <#if enableGenerateDoc>
    @ApiOperation(value = "更新数据", notes = "更新数据")
    @ApiOperationSupport(order = 5)
    </#if>
    @PutMapping(value = ["/modify"])
    fun modify(@Valid @RequestBody param: Update${className}POJO): ResponseEntity${r'<'}String> {
        val result: Boolean = ${className?uncap_first}Service.modify(param)
        return if (result) {
            ResponseEntity.failure()
        } else {
            ResponseEntity.success()
        }
    }

    /**
     * 逻辑删除
     *
     <#list pkColumns as column>
     * @param ${column.attrName} ${column.columnComment}
     </#list>
     * @param request
     * @return
     **/
    <#if enableGenerateDoc>
    @ApiOperation(value = "逻辑删除", notes = "逻辑删除")
    @ApiOperationSupport(order = 6)
    @ApiImplicitParams({
    <#list pkColumns as column>
        @ApiImplicitParam(name = "${column.attrName}", value = "${column.columnComment}", dataType = "${column.fullAttrType}", example = "f64ba455b9a8496a9007b50f39716253", required = true)<#if column_has_next>,</#if>
    </#list>
    })
    </#if>
    @DeleteMapping(value = ["/delete"])
    fun delete(<#list pkColumns as column>@RequestParam(value = "${column.attrName}") ${column.attrName}: ${column.attrType}, </#list>request: HttpServletRequest): ResponseEntity${r'<'}String> {
        val result: Boolean = ${className?uncap_first}Service.delete(<#list pkColumns as column>${column.attrName}<#if column_has_next>, </#if></#list>)
        return if (result) {
            ResponseEntity.failure()
        } else {
            ResponseEntity.success()
        }
    }
}
