package ${basePackage}.pojo

<#if enableGenerateDoc>
import io.swagger.annotations.ApiModel
import io.swagger.annotations.ApiModelProperty
</#if>
<#if haveBigDecimal>
import java.math.BigDecimal
</#if>
<#if haveDate>
import java.util.Date
</#if>

/**
 * @描述: ${comment}
 * @作者: ${author}
 * @创建时间: ${createTime}
 * @所属公司: ${company}
 */
<#if enableGenerateDoc>
@ApiModel(value = "${comment}(更新)")
</#if>
data class ${className}POJO (
    <#list columns as column>
    /**
     * ${column.columnComment}
     */
    <#if enableGenerateDoc>
    @ApiModelProperty(value = "${column.columnComment}<#if column.characterMaximumLength??>【允许最大长度：${column.characterMaximumLength}】</#if>", dataType = "${column.fullAttrType}", example = "${column.example}", required = ${column.required?string("true","false")})
    </#if>
    var ${column.attrName}: ${column.attrType}? = null<#if column_has_next>,</#if>
    </#list>
) : java.io.Serializable {
    companion object {
        private const val serialVersionUID = 1L
    }
}