# ${title}数据库设计文档

${description}

索引类型和默认值备注：
- PRI: 主键
- UNI: 唯一值
- MUL: 普通索引
- Empty String: 空字符串，不是NULL，是空字符串

<#list tableList as table>
### ${table_index + 1}. ${table.tableName}
- 表备注(comment):${table.comment}
- 存储引擎(engine):${table.engine}
- 排序规则(collate):${table.collation}

表设计说明:

|序号|字段名|字段类型|完整类型|是否为空|默认值|索引类型|字段备注|
|:---:|:---:|:---:|:---:|:---:|:---:|:---:|:---:|
<#list table.columns as column>
| ${column_index + 1} | ${column.columnName} | ${column.dataType} | ${column.columnType} | ${column.nullFlag} | ${column.defaultValue} | ${column.columnKey} | ${column.columnComment} |
</#list>

表DDL：
```sql

```


</#list>