package io.github.jyrmc.gmcp.common

/**
 * 模板路径常量
 *
 * @author jy
 * @date 2022/4/12 16:46
 **/
object FreemarkerConstant {

    // java代码模板路径常量
    const val BASIC_DIC = ""                                     // 模板所在文件夹-打包环境时使用
    const val CONTROLLER_TEMPLATE_JAVA = "Controller.java.ftl"   // controller模板
    const val RESTFUL_CONTROLLER_TEMPLATE_JAVA = "RestController.java.ftl"   // controller模板
    const val SERVICE_TEMPLATE_JAVA = "Service.java.ftl"         // service模板
    const val ENTITY_TEMPLATE_JAVA = "Entity.java.ftl"           // 实体模板
    const val ADD_POJO_TEMPLATE_JAVA = "AddDTO.java.ftl"     // 实体模板
    const val UPATE_POJO_TEMPLATE_JAVA = "UpdateDTO.java.ftl"      // 实体模板
    const val VO_TEMPLATE_JAVA = "VO.java.ftl"                   // 实体模板
    const val MAPPER_TEMPLATE_JAVA = "Mapper.java.ftl"           // dao层模板
    const val XML_TEMPLATE_JAVA = "Mapper.xml.ftl"               // 数据库访问层模板
    const val RESPONSE_TEMPLATE_JAVA = "ResponseEntity.java.ftl" // 结果返回模板

    // kotlin代码模板路径常量
    const val CONTROLLER_TEMPLATE_KOTLIN = "Controller.kt.ftl"   // controller模板
    const val SERVICE_TEMPLATE_KOTLIN = "Service.kt.ftl"         // service模板
    const val ISERVICE_TEMPLATE_KOTLIN = "IService.kt.ftl"       // service模板(mybatisplus)
    const val ENTITY_TEMPLATE_KOTLIN = "Entity.kt.ftl"           // 实体模板
    const val ADD_POJO_TEMPLATE_KOTLIN = "AddPOJO.kt.ftl"        // 实体模板
    const val UPDATE_POJO_TEMPLATE_KOTLIN = "UpdatePOJO.kt.ftl"  // 实体模板
    const val VO_TEMPLATE_KOTLIN = "VO.kt.ftl"                   // 实体模板
    const val MAPPER_TEMPLATE_KOTLIN = "Mapper.kt.ftl"           // dao层模板
    const val XML_TEMPLATE_KOTLIN = "Mapper.xml.ftl"             // 数据库访问层模板
    const val RESPONSE_TEMPLATE_KOTLIN = "ResponseEntity.kt.ftl" // 结果返回模板

    // 文件后缀名常量
    const val JAVA_FILE_SUFFIX = "java"
    const val KOTLIN_FILE_SUFFIX = "kt"
}