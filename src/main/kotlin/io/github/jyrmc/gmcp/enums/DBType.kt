package io.github.jyrmc.gmcp.enums

/**
 * 数据库类型枚举
 *
 * @author jy
 * @date 2023/1/12 11:28
 **/
enum class DBType(val dbName: String, val driverName: String) {
    MYSQL("mysql", com.mysql.cj.jdbc.Driver::class.java.name),
    PGSQL("postgresql", org.postgresql.Driver::class.java.name),
    TIDB("tidb", com.mysql.cj.jdbc.Driver::class.java.name),
    MARIADB("mariadb", com.mysql.cj.jdbc.Driver::class.java.name),
    MSSQL("tidb", com.microsoft.sqlserver.jdbc.SQLServerDriver::class.java.name),
    ORACLE("oracle", oracle.jdbc.OracleDriver::class.java.name),
    ;

    /**
     * 获取驱动信息
     *
     * @param driverName 驱动名称
     */
    fun getDBType(driverName: String): DBType {
        return when(driverName) {
            com.mysql.cj.jdbc.Driver::class.java.name -> MYSQL
            org.postgresql.Driver::class.java.name -> PGSQL
            com.microsoft.sqlserver.jdbc.SQLServerDriver::class.java.name -> MSSQL
            oracle.jdbc.OracleDriver::class.java.name -> ORACLE
            else -> MYSQL
        }
    }
}