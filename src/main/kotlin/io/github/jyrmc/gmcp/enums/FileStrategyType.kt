package io.github.jyrmc.gmcp.enums

/**
 * 文件生成策略
 *
 * @author jy
 * @since 2024/1/11 16:44
 **/
enum class FileStrategyType(val value: String) {
    // 如果已存在则不覆盖生成(默认值)
    CREATE_IF_NOT_EXISTS("createIfNotExists"),
    // 如果已存在则覆盖生成
    COVER("cover"),
    // 如果已存在文件则新建
    CREATE_NEW("createNew"),
    ;
}