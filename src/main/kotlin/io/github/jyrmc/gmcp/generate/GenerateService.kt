package io.github.jyrmc.gmcp.generate

import freemarker.template.Configuration
import freemarker.template.Template
import io.github.jyrmc.gmcp.common.FreemarkerConstant
import io.github.jyrmc.gmcp.entity.TableEntity
import io.github.jyrmc.gmcp.enums.FileStrategyType
import io.github.jyrmc.gmcp.utils.PrintInfoUtil
import java.io.*
import java.nio.charset.StandardCharsets

/**
 * 生成业务逻辑层
 *
 * @author jy
 * @date 2022/4/12 17:08
 * <p>
 * Blog: https://uniplore2000.gitee.io
 * Github: https://github.com/uniplore2000
 **/
object GenerateService {

    /**
     * 生成service-freemarker模板
     */
    fun freemarkerGenerate(
        fileGenStrategy: FileStrategyType,
        frameworkType: String,
        config: Configuration,
        projectPath: String,
        packageName: String,
        dic: String,
        languageType: String,
        tableInfo: TableEntity,
        dataMap: MutableMap<String, Any?>
    ) {
        // mybatis版本
        if (frameworkType == "mybatis") {
            var fileName =
                "${tableInfo.className}Service.${if (languageType == "java") FreemarkerConstant.JAVA_FILE_SUFFIX else FreemarkerConstant.KOTLIN_FILE_SUFFIX}"
            var fileFullPath =
                "${projectPath}/src/main/${languageType}/${packageName.replace(".", "/")}/${dic}/${fileName}"

            var file = File(fileFullPath)
            if (fileGenStrategy == FileStrategyType.CREATE_IF_NOT_EXISTS) {
                if (file.exists()) {
                    println("message: file [${fileName}] is exists.")
                    return
                }
            } else if (fileGenStrategy == FileStrategyType.COVER) {
                file.delete()
                file.createNewFile()
            } else {
                if (file.exists()) {
                    fileName =
                        "${tableInfo.className}Service_${System.currentTimeMillis()}.${if (languageType == "java") FreemarkerConstant.JAVA_FILE_SUFFIX else FreemarkerConstant.KOTLIN_FILE_SUFFIX}"
                    fileFullPath =
                        "${projectPath}/src/main/${languageType}/${packageName.replace(".", "/")}/${dic}/${fileName}"
                }
                file = File(fileFullPath)
            }

            if (!file.exists()) {
                file.parentFile.mkdirs()

                PrintInfoUtil.printCreateFold(file.parentFile.absolutePath)
            }

            val template: Template = if (languageType == "java") {
                config.getTemplate(
                    "${FreemarkerConstant.BASIC_DIC}/java/$frameworkType/${FreemarkerConstant.SERVICE_TEMPLATE_JAVA}",
                    "UTF-8"
                )
            } else {
                config.getTemplate(
                    "${FreemarkerConstant.BASIC_DIC}/kotlin/$frameworkType/${FreemarkerConstant.SERVICE_TEMPLATE_KOTLIN}",
                    "UTF-8"
                )
            }

            val out: Writer = BufferedWriter(OutputStreamWriter(FileOutputStream(fileFullPath), StandardCharsets.UTF_8))
            template.process(dataMap, out)
            out.flush()
            out.close()

            PrintInfoUtil.printCreateFile(fileFullPath)
        } else {// mybatisplus版本
            // service
            var fileName =
                "${tableInfo.className}Service.${if (languageType == "java") FreemarkerConstant.JAVA_FILE_SUFFIX else FreemarkerConstant.KOTLIN_FILE_SUFFIX}"
            var fileFullPath =
                "${projectPath}/src/main/${languageType}/${packageName.replace(".", "/")}/${dic}/${fileName}"

            var file = File(fileFullPath)
            if (fileGenStrategy == FileStrategyType.CREATE_IF_NOT_EXISTS) {
                if (file.exists()) {
                    println("message: file [${fileName}] is exists.")
                    return
                }
            } else if (fileGenStrategy == FileStrategyType.COVER) {
                file.delete()
                file.createNewFile()
            } else {
                if (file.exists()) {
                    fileName =
                        "${tableInfo.className}Service_${System.currentTimeMillis()}.${if (languageType == "java") FreemarkerConstant.JAVA_FILE_SUFFIX else FreemarkerConstant.KOTLIN_FILE_SUFFIX}"
                    fileFullPath =
                        "${projectPath}/src/main/${languageType}/${packageName.replace(".", "/")}/${dic}/${fileName}"
                }
                file = File(fileFullPath)
            }

            if (!file.exists()) {
                file.parentFile.mkdirs()

                PrintInfoUtil.printCreateFold(file.parentFile.absolutePath)
            }

            val template = if (languageType == "java") {
                config.getTemplate(
                    "${FreemarkerConstant.BASIC_DIC}/java/$frameworkType/${FreemarkerConstant.SERVICE_TEMPLATE_JAVA}",
                    "UTF-8"
                )
            } else {
                config.getTemplate(
                    "${FreemarkerConstant.BASIC_DIC}/kotlin/$frameworkType/${FreemarkerConstant.SERVICE_TEMPLATE_KOTLIN}",
                    "UTF-8"
                )
            }

            val out: Writer = BufferedWriter(OutputStreamWriter(FileOutputStream(fileFullPath), StandardCharsets.UTF_8))
            template.process(dataMap, out)
            out.flush()
            out.close()

            PrintInfoUtil.printCreateFile(fileFullPath)
        }
    }
}