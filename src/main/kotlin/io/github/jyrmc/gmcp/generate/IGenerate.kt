package io.github.jyrmc.gmcp.generate

import freemarker.template.Configuration
import io.github.jyrmc.gmcp.entity.TableEntity
import io.github.jyrmc.gmcp.enums.FileStrategyType

/**
 * 接口定义
 *
 * @author jy
 * @date 2022/4/12 16:53
 * <p>
 * Blog: https://uniplore2000.gitee.io
 * Github: https://github.com/uniplore2000
 **/
interface IGenerate {

    /**
     * 生成方法
     *
     * @param frameworkType 对应生成框架名称：mybatis、mybatisplus
     * @param config 配置
     * @param projectPath 项目所在文件夹
     * @param dic 目录
     * @param languageType 语言类型：java、kotlin
     * @param tableInfo 表信息
     * @param dataMap 其他数据
     */
    fun freemarkerGenerate(
        fileGenStrategy: FileStrategyType,
        frameworkType: String,
        config: Configuration,
        projectPath: String,
        packageName: String,
        dic: String,
        languageType: String,
        tableInfo: TableEntity,
        dataMap: MutableMap<String, Any?>
    )
}