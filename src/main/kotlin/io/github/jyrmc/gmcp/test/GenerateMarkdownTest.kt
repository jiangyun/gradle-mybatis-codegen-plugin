package cn.uniplore.codegen.test

import io.github.jyrmc.gmcp.utils.GenerateMarkdownUtil

fun main() {
    GenerateMarkdownUtil.export(
        dbType = "mysql",
        url = "jdbc:mysql://192.168.2.72:3306/uniplore-data-studio?useUnicode=true&serverTimezone=Asia/Shanghai&useSSL=false&characterEncoding=UTF-8&allowPublicKeyRetrieval=true&autoReconnect=true&failOverReadOnly=false&connectionCollation=utf8mb4_general_ci",
        databaseName = "uniplore-data-studio",
        userName = "root",
        password = "Uniplore!@#123",
        projectPath = "D:\\IdeaProjects\\gradle-mybatis-codegen-plugin"
    )
}