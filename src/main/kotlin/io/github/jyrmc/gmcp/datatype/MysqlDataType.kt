package io.github.jyrmc.gmcp.datatype

/**
 * 创建于 2020-05-26 23:01
 *
 * @author jiangyun
 * @类说明：mysql数据类型
 */
object MysqlDataType {

    /**
     * 获取mysql字段类型信息
     *
     * @param languageType 语言类型(java,kotlin)
     * @return
     */
    fun getMysqlParamType(languageType: String): MutableMap<String, String> {
        val type: MutableMap<String, String> = mutableMapOf()
        type["char"] = "String"
        type["varchar"] = "String"
        type["tinytext"] = "String"
        type["text"] = "String"
        type["mediumtext"] = "String"
        type["longtext"] = "String"
        type["tinyint"] = if (languageType == "java") "Integer" else "Int"
        type["smallint"] = if (languageType == "java") "Integer" else "Int"
        type["mediumint"] = if (languageType == "java") "Integer" else "Int"
        type["int"] = if (languageType == "java") "Integer" else "Int"
        type["integer"] = if (languageType == "java") "Integer" else "Int"
        type["bigint"] = "Long"
        type["float"] = "Float"
        type["double"] = "Double"
        type["decimal"] = "BigDecimal"
        type["time"] = if (languageType == "java") "Integer" else "Int"
        type["date"] = "LocalDateTime"
        type["datetime"] = "LocalDateTime"
        type["timestamp"] = "LocalDateTime"
        type["json"] = "com.alibaba.fastjson.JSONObject"
        type["blob"] = "String"
        type["longblob"] = "String"
        type["tinyblob"] = "String"
        return type
    }

    /**
     * 获取mysql字段完整类型信息
     *
     * @param languageType 语言类型(java,kotlin) 下面的语言为kotlin时，为了文档显示字段正常，还是使用的java标识的类型
     * @return
     */
    fun getFullMysqlParamType(languageType: String): MutableMap<String, String> {
        val fullType: MutableMap<String, String> = mutableMapOf()
        fullType["char"] = if (languageType == "java") "java.lang.String" else "java.lang.String"
        fullType["varchar"] = if (languageType == "java") "java.lang.String" else "java.lang.String"
        fullType["tinytext"] = if (languageType == "java") "java.lang.String" else "java.lang.String"
        fullType["text"] = if (languageType == "java") "java.lang.String" else "java.lang.String"
        fullType["mediumtext"] = if (languageType == "java") "java.lang.String" else "java.lang.String"
        fullType["longtext"] = if (languageType == "java") "java.lang.String" else "java.lang.String"
        fullType["tinyint"] = if (languageType == "java") "java.lang.Integer" else "java.lang.Integer"
        fullType["smallint"] = if (languageType == "java") "java.lang.Integer" else "java.lang.Integer"
        fullType["mediumint"] = if (languageType == "java") "java.lang.Integer" else "java.lang.Integer"
        fullType["int"] = if (languageType == "java") "java.lang.Integer" else "java.lang.Integer"
        fullType["integer"] = if (languageType == "java") "java.lang.Integer" else "java.lang.Integer"
        fullType["bigint"] = if (languageType == "java") "java.lang.Long" else "java.util.Long"
        fullType["float"] = if (languageType == "java") "java.lang.Float" else "java.util.Float"
        fullType["double"] = if (languageType == "java") "java.lang.Double" else "java.util.Double"
        fullType["decimal"] = "java.math.BigDecimal"
        fullType["date"] = if (languageType == "java") "java.lang.Long" else "java.lang.Long"
        fullType["time"] = if (languageType == "java") "java.lang.Long" else "java.lang.Long"
        fullType["datetime"] = if (languageType == "java") "java.lang.Long" else "java.lang.Long"
        fullType["timestamp"] = if (languageType == "java") "java.lang.Long" else "java.lang.Long"
        fullType["json"] = if (languageType == "java") "com.alibaba.fastjson.JSONObject" else "com.alibaba.fastjson.JSONObject"
        fullType["blob"] = if (languageType == "java") "java.lang.String" else "java.lang.String"
        fullType["longblob"] = if (languageType == "java") "java.lang.String" else "java.lang.String"
        fullType["tinyblob"] = if (languageType == "java") "java.lang.String" else "java.lang.String"
        return fullType
    }
}