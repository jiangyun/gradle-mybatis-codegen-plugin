package io.github.jyrmc.gmcp.entity

/**
 * 创建于 2020-01-08 21:14
 *
 * @author jiangyun
 * @类说明：表对应的列名
 */
data class ColumnEntity(
    var columnName: String? = null,             // 列名
    var dataType: String? = null,               // 数据类型
    var columnDefault: String? = null,          // 列默认值
    var isNullable: String? = null,             // 是否必填
    var characterMaximumLength: String? = "",   // 字段最大长度
    var columnComment: String? = null,          // 字段备注
    var columnKey: String? = null,              // 是否是主键
    var columnType: String? = null,             // 列类型
    var extra: String? = null,                  // 附加信息

    // 以下为拓展属性
    var nullFlag: String = "",                  // 是否否空(YES,NO)
    var defaultValue: String = "",              // markdown显示的默认值
    var attrName: String? = null,               // 字段驼峰名称
    var upcaseAttrName: String? = null,         // 首字母大写的字段名称
    var attrType: String? = null,               // 对应java或kotlin数据类型
    var fullAttrType: String? = null,           // 对应java或kotlin数据类型
    var example: String? = null,                // 示例，主要针对接口文档使用
    var required: Boolean? = null               // 是否必填，主要针对接口文档使用
)