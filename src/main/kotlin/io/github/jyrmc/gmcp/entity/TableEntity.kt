package io.github.jyrmc.gmcp.entity

/**
 * 创建于 2020-01-08 21:13
 *
 * @author jiangyun
 * @类说明：表实体
 */
data class TableEntity(
    var tableName: String? = null,                  // 表名称
    var comment: String? = null,                    // 表注释
    var collation: String? = null,                  // 字符集编码
    var engine: String? = null,                     // 存储引擎
    var defaultCharset: String? = null,             // 默认字符集编码
    var rowFormat: String? = null,                  // 行格式化
    var columns: MutableList<ColumnEntity>? = null, // 列

    // 以下为拓展属性
    var haveBigDecimal: Boolean? = false,           // 是否有decimal数据类型
    var haveLocalDate: Boolean? = false,                 // 是否有日期数据类型
    var haveLocalDateTime: Boolean? = false,                 // 是否有日期数据类型
    var haveLocalTime: Boolean? = false,                 // 是否有日期数据类型
    var haveLogicDelField: Boolean? = false,                 // 是否有逻辑删除字段
    var havaJsonData: Boolean? = false,                 // 是否有JSON类型字段
    var className: String? = null,                  // 类名
    var createTime: String? = null,                 // 创建时间
    var author: String? = null,                     // 作者
    var pkColumns: MutableList<ColumnEntity>? = null, // 主键列
    var uniColumns: MutableList<ColumnEntity>? = null, // 唯一值列
    var mulColumns: MutableList<ColumnEntity>? = null, // 普通索引列
//    var ignoreColumns: MutableList<String>? = null  // 需要忽略的列
)