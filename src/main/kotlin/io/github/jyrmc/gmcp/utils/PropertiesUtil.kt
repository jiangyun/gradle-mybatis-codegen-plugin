package io.github.jyrmc.gmcp.utils


/**
 * 构造数据库连接属性
 * @author jy
 * @date 2022/1/15 下午1:52
 */
object PropertiesUtil {

    /**
     * 获取Mysql连接属性
     *
     * @param url      连接字符串
     * @param username 用户名
     * @param password 密码
     */
    fun getMysqlConnectionProperties(url: String, username: String, password: String): MutableMap<String, String> {
        val properties: MutableMap<String, String> = mutableMapOf()
        // properties["driverClassName"] = "com.mysql.cj.jdbc.Driver"
        properties["driverClassName"] = com.mysql.cj.jdbc.Driver::class.java.name
        properties["url"] = url
        properties["username"] = username
        properties["password"] = password

        return properties
    }

    /**
     * 获取pgsql连接属性
     *
     * @param url      连接字符串
     * @param username 用户名
     * @param password 密码
     */
    fun getPgsqlConnectionProperties(url: String, username: String, password: String): MutableMap<String, String> {
        val properties: MutableMap<String, String> = mutableMapOf()
        properties["driverClassName"] = org.postgresql.Driver::class.java.name
        properties["url"] = url
        properties["username"] = username
        properties["password"] = password

        return properties
    }

    /**
     * 获取Oracle连接属性
     *
     * @param url      连接字符串
     * @param username 用户名
     * @param password 密码
     */
    fun getOracleConnectionProperties(url: String, username: String, password: String): MutableMap<String, String> {
        val properties: MutableMap<String, String> = mutableMapOf()
        properties["driverClassName"] = oracle.jdbc.OracleDriver::class.java.name
        properties["url"] = url
        properties["username"] = username
        properties["password"] = password

        return properties
    }

    /**
     * 获取mssql连接属性
     *
     * @param url      连接字符串
     * @param username 用户名
     * @param password 密码
     */
    fun getMsSqlConnectionProperties(url: String, username: String, password: String): MutableMap<String, String> {
        val properties: MutableMap<String, String> = mutableMapOf()
        properties["driverClassName"] = com.microsoft.sqlserver.jdbc.SQLServerDriver::class.java.name
        properties["url"] = url
        properties["username"] = username
        properties["password"] = password

        return properties
    }

    /**
     * 获取Tidb连接属性
     *
     * @param url      连接字符串
     * @param username 用户名
     * @param password 密码
     */
    fun getTiDBConnectionProperties(url: String, username: String, password: String): MutableMap<String, String> {
        val properties: MutableMap<String, String> = mutableMapOf()
        properties["driverClassName"] = com.mysql.cj.jdbc.Driver::class.java.name
        properties["url"] = url
        properties["username"] = username
        properties["password"] = password

        return properties
    }

}