package io.github.jyrmc.gmcp.utils

import io.github.jyrmc.gmcp.generate.GenerateVo
import io.github.jyrmc.gmcp.datatype.MysqlDataType
import io.github.jyrmc.gmcp.datatype.PostgresqlDataType
import io.github.jyrmc.gmcp.entity.ColumnEntity
import io.github.jyrmc.gmcp.entity.TableEntity
import io.github.jyrmc.gmcp.generate.*
import freemarker.template.Configuration
import freemarker.template.DefaultObjectWrapper
import io.github.jyrmc.gmcp.enums.FileStrategyType
import org.gradle.configurationcache.extensions.capitalized
import java.text.SimpleDateFormat
import java.util.*


/**
 * 创建于 2020-01-08 18:16
 *
 * @author jiangyun
 * @类说明：代码生成工具类
 */
object GenerateCodeUtil {

    /**
     * 生成代码
     * @param dbType                数据类型(如：mysql、postgresql)
     * @param url                   数据库连接地址
     * @param databaseName          数据库名称
     * @param userName              用户名
     * @param password              密码
     * @param frameworkType         框架类型(mybatis或mybatisplus)
     * @param tablePrefix           表前缀
     * @param tableNames            表名称
     * @param ignoreColumns         需要忽略的列
     * @param projectPath           项目绝对路径
     * @param packageName           包名称
     * @param controller            控制器文件夹名称
     * @param service               业务层文件夹名称
     * @param mapper                dao层文件夹名称
     * @param entity                实体文件夹名称
     * @param xml                   dao层对应xml文件夹名称
     * @param languageType          语言类型，目前只支持java和kotlin
     * @param author                作者
     * @param apiPrefix             接口前缀
     * @param restfulStyle          是否controller使用resfful风格
     * @param swagger               是否允许生成swagger doc
     * @param fileGenStrategy       文件生成策略：createIfNotExists-如果已存在则不覆盖生成(默认值)，cover-如果已存在则覆盖生成，createNew-如果已存在文件则新建
     */
    @Throws(Exception::class)
    fun generateCode(
        dbType: String,
        url: String,
        databaseName: String,
        userName: String,
        password: String,
        frameworkType: String,
        tablePrefix: String,
        tableNames: MutableList<String>,
        ignoreColumns: MutableList<String>,
        projectPath: String,
        packageName: String,
        controller: String,
        service: String,
        mapper: String,
        entity: String,
        xml: String,
        languageType: String,
        author: String,
        apiPrefix: String,
        restfulStyle: Boolean,
        swagger: Boolean,
        fileGenStrategy: FileStrategyType,
    ) {
        val attrTypes: Map<String, String>? = when (dbType) {
            "mysql" -> MysqlDataType.getMysqlParamType(languageType)
            "postgresql" -> PostgresqlDataType.getPostgresqlParamType(languageType)
            else -> null
        }
        val fullAttrTypes: Map<String, String>? = when (dbType) {
            "mysql" -> MysqlDataType.getFullMysqlParamType(languageType)
            "postgresql" -> PostgresqlDataType.getFullPostgresqlParamType(languageType)
            else -> null
        }
        if (tableNames.isNotEmpty()) {
            val tables: MutableList<TableEntity> = mutableListOf()
            var table: TableEntity?
            tableNames.forEach {
                println("start generate table: ${it}")
                if (!it.isBlank()) {
                    // 获取表信息
                    table = when (dbType) {
                        "mysql" -> MysqlDatabaseUtil.getTableInfo(url, databaseName, userName, password, it)
                        "postgresql" -> PostgresqlDatabaseUtil.getTableInfo(url, userName, password, it)
                        else -> null
                    }
                    if (table != null && !table!!.tableName.isNullOrBlank()) {
                        table!!.className = it.let { UnderlineToCamelUtil.underlineToCamel(it, tablePrefix) } // 避免异常处理
                        table!!.createTime = SimpleDateFormat("yyyy/MM/dd HH:mm").format(Date())
                        table!!.author = author
                        tables.add(table!!)
                    } else {
                        println("get table $it error,can't generate this table code")
                    }
                }
            }

            var haveBigDecimal = false
            var haveLocalDate = false
            var haveLocalDateTime = false
            var haveLocalTime = false
            var haveLogicDelField = false
            var havaJsonData = false
            var columns: MutableList<ColumnEntity>?
            var pkColumns: MutableList<ColumnEntity>?
            for (index in 0 until tables.size) {
                // 获取表列
                columns = when (dbType) {
                    "mysql" -> MysqlDatabaseUtil.findTableColumns(
                        url,
                        databaseName,
                        userName,
                        password,
                        tables[index].tableName!!
                    )
                    "postgresql" -> PostgresqlDatabaseUtil.findTableColumns(
                        url,
                        userName,
                        password,
                        tables[index].tableName!!
                    )
                    else -> null
                }

                // 初始化主键列数组
                pkColumns = mutableListOf()

                // 忽略指定字段
                if (ignoreColumns.isEmpty()) {
                    ignoreColumns.add("tenant_id")
                    ignoreColumns.add("tenantId")
                    ignoreColumns.add("tenantID")
                    ignoreColumns.add("tenant")
                    ignoreColumns.add("sys_group_id")
                }

                ignoreColumns.apply {
                    distinct()
                }.forEach {column ->
                    columns!!.removeAll(columns.filter { it.columnName == column });
                }

                // 处理列数据
                for (subIndex in 0 until columns!!.size) {// 处理一些值及默认数据
                    columns[subIndex].attrName =
                        UnderlineToCamelUtil.underlineToCamel(columns[subIndex].columnName!!, true)
                    columns[subIndex].upcaseAttrName =
                        UnderlineToCamelUtil.firstCharToUpcaseOrLowcase(columns[subIndex].attrName!!, false)
                    columns[subIndex].attrType = attrTypes!!.getOrDefault(columns[subIndex].dataType, "Object")
                    columns[subIndex].fullAttrType = fullAttrTypes!!.getOrDefault(
                        columns[subIndex].dataType,
                        if (languageType == "java") "java.lang.Object" else "Object"
                    )
                    if (columns[subIndex].columnName == "deleted") {
                        haveLogicDelField = true
                    }
                    if (columns[subIndex].attrType!! == "LocalDateTime") {
                        haveLocalDateTime = true
                    }
                    if (columns[subIndex].attrType!! == "LocalDate") {
                        haveLocalDate = true
                    }
                    if (columns[subIndex].attrType!! == "LocalTime") {
                        haveLocalTime = true
                    }
                    if (columns[subIndex].attrType!! == "BigDecimal") {
                        haveBigDecimal = true
                    }
                    if (columns[subIndex].attrType!! == "com.alibaba.fastjson.JSONObject") {
                        havaJsonData = true
                    }
                    columns[subIndex].required = columns[subIndex].isNullable == "NO"
                    if (columns[subIndex].attrType!! == "Date" || columns[subIndex].attrType!! == "LocalDateTime" || columns[subIndex].attrType!! == "LocalDate") {
                        columns[subIndex].example = System.currentTimeMillis().toString()
                    } else if (columns[subIndex].attrType!! != "Date" && !columns[subIndex].columnDefault.isNullOrBlank()) {
                        columns[subIndex].example = columns[subIndex].columnDefault
                    } else {
                        if (columns[subIndex].attrType!! == "LocalTime") {
                            columns[subIndex].attrType = "java.time.LocalTime"
                            columns[subIndex].example = "1651794894368"
                        }
                        if (columns[subIndex].attrType!! == "com.alibaba.fastjson.JSONObject") {
                            columns[subIndex].example = """{\"id:\":\"11465456\",\"name\":\"test\"}"""
                        }
                        if (columns[subIndex].attrType!! == "Integer") {
                            columns[subIndex].example = "1"
                        }
                        if (columns[subIndex].attrType!! == "Long") {
                            columns[subIndex].example = "1000000000"
                        }
                        if (columns[subIndex].attrType!! == "BigDecimal") {
                            columns[subIndex].example = "1500.5"
                        }
                        if (columns[subIndex].attrType!! == "Boolean") {
                            columns[subIndex].example = "true"
                        }
                        if (columns[subIndex].attrType!! == "Float") {
                            columns[subIndex].example = "12.5"
                        }
                        if (columns[subIndex].attrType!! == "Double") {
                            columns[subIndex].example = "12.55"
                        }
                        if (!columns[subIndex].columnKey.isNullOrBlank() && columns[subIndex].columnKey == "PRI") {
                            columns[subIndex].columnType = when (columns[subIndex].columnType) {
                                "varchar" -> "58fbed7334444a19a27e0c5082445658"
                                "bigint" -> "9898765434565434565"
                                "int" -> "123"
                                "tinyint" -> "12"
                                else -> ""
                            }
                            columns[subIndex].required = true
                            pkColumns.add(columns[subIndex])
                        }
                        if (columns[subIndex].attrType!! == "String") {
                            columns[subIndex].example = "字符串数据"
                            if (columns[subIndex].columnType!!.contentEquals("blob")) {
                                columns[subIndex].example = "二进制内容，需定义转换类进行转换，本代码生产插件不负责转换处理！"
                            }
                            if (columns[subIndex].attrName!!.contains("valid")) {
                                columns[subIndex].example = "1"
                            }
                            if (columns[subIndex].attrName!!.contains("remark")) {
                                columns[subIndex].example = "这里填写备注信息..."
                            }
                            if (columns[subIndex].attrName!!.contains("createBy") || columns[subIndex].attrName!!.contains(
                                    "updateBy"
                                )
                            ) {
                                columns[subIndex].example = "88fadd7334a44a19a27e0c5082405853"
                            }
                            if (columns[subIndex].attrName!!.contains("phone") || columns[subIndex].attrName!!.contains(
                                    "Phone"
                                )
                            ) {
                                columns[subIndex].example = "18780000030"
                            }
                            if (columns[subIndex].attrName!!.contains("email") || columns[subIndex].attrName!!.contains(
                                    "Email"
                                )
                            ) {
                                columns[subIndex].example = "test123456@163.com"
                            }
                            if (columns[subIndex].attrName!!.contains("userName") || columns[subIndex].attrName!!.contains(
                                    "UserName"
                                )
                            ) {
                                columns[subIndex].example = "张三"
                            }
                            if (columns[subIndex].attrName!!.contains("Name") || columns[subIndex].attrName!!.contains("name")) {
                                columns[subIndex].example = "名称"
                            }
                            if (columns[subIndex].attrName!!.contains("address") || columns[subIndex].attrName!!.contains(
                                    "Address"
                                )
                            ) {
                                columns[subIndex].example = "贵州省贵阳市观山湖区林城西路"
                            }
                            if (columns[subIndex].attrName!! == "password" || columns[subIndex].attrName!!.contains("Password") || columns[subIndex].attrName!!.contains(
                                    "pwd"
                                )
                            ) {
                                columns[subIndex].example = "08fbed7334444a19a27e0c5082405853"
                            }
                        }
                    }
                }
                tables[index].haveBigDecimal = haveBigDecimal
                tables[index].haveLocalDate = haveLocalDate
                tables[index].haveLocalDateTime = haveLocalDateTime
                tables[index].haveLocalTime = haveLocalTime
                tables[index].haveLogicDelField = haveLogicDelField
                tables[index].havaJsonData = havaJsonData
                tables[index].columns = columns
                tables[index].pkColumns = pkColumns
                // tables[index].ignoreColumns = ignoreColumns
            }

            val config = Configuration(Configuration.VERSION_2_3_23)
            config.setClassLoaderForTemplateLoading(this@GenerateCodeUtil.javaClass.classLoader, "templates/")
            config.setEncoding(Locale.CHINA, "utf-8")
            config.objectWrapper = DefaultObjectWrapper(Configuration.VERSION_2_3_23)

            var dataMap: MutableMap<String, Any?>
            tables.forEach {
                dataMap = mutableMapOf()
                dataMap["enableGenerateDoc"] = swagger
                dataMap["tableName"] = it.tableName
                dataMap["comment"] = it.comment
                dataMap["columns"] = it.columns
                dataMap["pkColumns"] = it.pkColumns
                dataMap["haveBigDecimal"] = it.haveBigDecimal
                dataMap["haveLocalDate"] = it.haveLocalDate
                dataMap["haveLocalDateTime"] = it.haveLocalDateTime
                dataMap["haveLocalTime"] = it.haveLocalTime
                dataMap["haveLogicDelField"] = it.haveLogicDelField
                dataMap["havaJsonData"] = it.havaJsonData
                dataMap["className"] = it.className!!.capitalized()
                dataMap["createTime"] = it.createTime
                dataMap["author"] = it.author
                dataMap["basePackage"] = packageName
                dataMap["controller"] = controller
                dataMap["service"] = service
                dataMap["mapper"] = mapper
                dataMap["entity"] = entity
                dataMap["apiPrefix"] = apiPrefix
                it.className = it.className!!.capitalized()

                // 生成实体
                GenerateEntity.freemarkerGenerate(
                    fileGenStrategy,
                    frameworkType,
                    config,
                    projectPath,
                    packageName,
                    entity,
                    languageType,
                    it,
                    dataMap
                )

                // 移除逻辑删除字段
                dataMap["columns"] = it.columns?.filter { subIt -> subIt.columnName != "deleted" }

                // 生成新增pojo
                GenerateAddDto.freemarkerGenerate(
                    fileGenStrategy,
                    frameworkType,
                    config,
                    projectPath,
                    packageName,
                    entity,
                    languageType,
                    it,
                    dataMap
                )
                // 生成更新pojo
                GenerateUpdateDto.freemarkerGenerate(
                    fileGenStrategy,
                    frameworkType,
                    config,
                    projectPath,
                    packageName,
                    entity,
                    languageType,
                    it,
                    dataMap
                )
                // 生成vo
                GenerateVo.freemarkerGenerate(
                    fileGenStrategy,
                    frameworkType,
                    config,
                    projectPath,
                    packageName,
                    entity,
                    languageType,
                    it,
                    dataMap
                )
                // 生成mapper
                GenerateMapper.freemarkerGenerate(
                    fileGenStrategy,
                    frameworkType,
                    config,
                    projectPath,
                    packageName,
                    mapper,
                    languageType,
                    it,
                    dataMap
                )
                // 生成mapperXml
                GenerateMapperXml.freemarkerGenerate(
                    fileGenStrategy,
                    frameworkType,
                    config,
                    projectPath,
                    xml,
                    languageType,
                    it,
                    dataMap
                )
                // 生成service
                GenerateService.freemarkerGenerate(
                    fileGenStrategy,
                    frameworkType,
                    config,
                    projectPath,
                    packageName,
                    service,
                    languageType,
                    it,
                    dataMap
                )
                // 生成controller
                GenerateController.freemarkerGenerate(
                    fileGenStrategy,
                    restfulStyle,
                    frameworkType,
                    config,
                    projectPath,
                    packageName,
                    controller,
                    languageType,
                    it,
                    dataMap
                )
            }
            println(
                "____ ____ _  _ ____ ____ ____ ___ ____    ____ ____ ___  ____    ____ _  _ ____ ____ ____ ____ ____      /\n" +
                        "| __ |___ |\\ | |___ |__/ |__|  |  |___    |    |  | |  \\ |___    [__  |  | |    |    |___ [__  [__      / \n" +
                        "|__] |___ | \\| |___ |  \\ |  |  |  |___    |___ |__| |__/ |___    ___] |__| |___ |___ |___ ___] ___]    .  \n" +
                        "                                                                                                          "
            )
        } else {
            println("unavailable table or table not exists or running failure")
        }
    }
}