import io.github.jyrmc.gmcp.task.GenerateCodeTask
import io.github.jyrmc.gmcp.utils.GenerateCodeUtil
import org.gradle.api.Project
import org.gradle.internal.impldep.org.junit.Test
import org.gradle.testfixtures.ProjectBuilder
import kotlin.test.assertTrue

class CodegenPluginTest {
    @Test
    fun greeterPluginTaskToProject() {
        val project: Project = ProjectBuilder.builder().build()
        project.pluginManager.apply("gradle-mybatis-codegen-plugin")
        assertTrue(project.tasks.getByName("generateCode") is GenerateCodeTask)
    }


    @Test
    fun generateCodeTest() {
        val url =
            "jdbc:mysql://localhost:3306/uniplore-micro-basic?useUnicode=true&serverTimezone=Asia/Shanghai&useSSL=false&characterEncoding=UTF-8&allowPublicKeyRetrieval=true&autoReconnect=true&failOverReadOnly=false&tinyInt1isBit=false"
        GenerateCodeUtil.generateCode(
            "mysql",
            url,
            "uniplore-micro-basic",
            "root",
            "admin123456",
            "mybatisplus",
            "uniplore",
            arrayListOf("uniplore_sys_user"),
            "/Users/jiangyun/IdeaProjects/gmcp-demo",
            "com.example.gmcp",
            "controller",
            "serevice",
            "mapper",
            "entity",
            "mapper",
            "java",
            "jy",
            "",
            true,
            true
        )
    }
}