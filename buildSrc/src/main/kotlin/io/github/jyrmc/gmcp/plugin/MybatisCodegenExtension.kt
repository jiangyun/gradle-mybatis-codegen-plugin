package io.github.jyrmc.gmcp.plugin

import org.gradle.api.provider.Property

interface MybatisCodegenExtension {
    val dbType: Property<String>                 // 数据库类型
    val url: Property<String>                    // 数据库连接地址
    val port: Property<Int>                      // 端口
    val database: Property<String>               // 数据库名称
    val userName: Property<String>               // 用户名
    val password: Property<String>               // 密码
    val frameworkType: Property<String>          // 框架类型(mybatis或mybatisplus)
    val tablePrefix: Property<String>            // 表前缀
    val tables: Property<MutableList<String>>    // 表名称
    val projectPath: Property<String>            // 项目绝对路径
    val packageName: Property<String>            // 包名称
    val controller: Property<String>             // 控制器文件夹名称
    val service: Property<String>                // 业务层文件夹名称
    val mapper: Property<String>                 // dao层文件夹名称
    val entity: Property<String>                 // 实体文件夹名称
    val xml: Property<String>                    // dao层对应xml文件夹名称
    val languageType: Property<String>           // 语言类型，目前只支持java和kotlin
    val author: Property<String>                 // 作者
    val apiPrefix: Property<String>              // 接口前缀
    val restfulStyle: Property<Boolean>         // 是否生成restful接口
    val swagger: Property<Boolean>              // 是否允许生成doc
}