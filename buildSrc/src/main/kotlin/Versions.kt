/**
 * 版本申明
 */
object Versions {
    const val freemarkerVersion = "2.3.31"
    const val druidVersion = "1.2.15"
    const val mysqlDriverVersion = "8.0.30"
    const val pgsqlDriverVersion = "42.5.1"
    const val mssqlDriverVersion = "11.2.1.jre11"
    const val oracleDriverVersion = "21.7.0.0"
    const val pluginPublishingVersion = "1.1.0"
}