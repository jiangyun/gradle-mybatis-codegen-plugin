# Controller中Result源码

```java
package com.iwiteks.core.common.web;

import com.fasterxml.jackson.annotation.JsonInclude;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;

import java.io.Serializable;
import java.util.Map;

/**
 * 统一响应消息
 *
 * @author jy
 * @since 2022/4/24 14:20
 **/
@Slf4j
@Data
@NoArgsConstructor
@ApiModel(value = "统一响应消息报文")
public class Result<T> implements Serializable {
    private static final long serialVersionUID = 1L;
    public static final String DEFAULT_ERROR_MSG = "接口不能使用map类型作为返回值，请联系接口开发人员进行调整！"; // 默认错误信息

    /**
     * 返回码
     */
    @ApiModelProperty(value = "返回码", dataType = "java.lang.Integer", example = "200")
    private int code;
    /**
     * 返回信息
     */
    @ApiModelProperty(value = "返回信息", dataType = "java.lang.String", example = "成功")
    private String msg;
    /**
     * 时间戳
     */
    @ApiModelProperty(value = "时间戳", dataType = "java.lang.Long", example = "14945646546985")
    private long time;
    /**
     * 返回的数据
     */
    @ApiModelProperty(value = "返回的数据")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private T data;
    /**
     * 数据长度
     */
    @ApiModelProperty(value = "数据长度", dataType = "java.lang.Long", example = "1000")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private Long total;

    /**
     * 构造函数
     *
     * @param resultCode 返回码枚举
     * @param data       数据
     * @param total      数据条数
     */
    private Result(ResultCode resultCode, T data, Long total) {
        this.time = System.currentTimeMillis();
        this.code = resultCode.getCode();
        this.msg = resultCode.getMsg();
        this.data = data;
        this.total = total;
    }

    /**
     * 构造函数
     *
     * @param code  返回码
     * @param msg   返回信息
     * @param data  数据
     * @param total 数据条数
     */
    private Result(int code, String msg, T data, Long total) {
        this.time = System.currentTimeMillis();
        this.code = code;
        this.msg = msg;
        this.data = data;
        this.total = total;
    }

    /**
     * 成功
     *
     * @param <T> 数据类型
     * @return 但不返回任何数据，只返回成功的状态码
     */
    public static <T> Result<T> success() {
        return new Result<>(ResultCode.SUCCESS, null, null);
    }

    /**
     * 自定义返回码成功
     *
     * @param <T> 数据类型
     * @return 但不返回任何数据，只返回成功的状态码
     */
    public static <T> Result<T> success(String msg) {
        return new Result<>(ResultCode.SUCCESS.getCode(), msg, null, null);
    }

    /**
     * 成功
     *
     * @param data 数据
     * @param <T>  数据类型
     * @return 返回数据
     */
    public static <T> Result<T> success(T data) {
        if (data instanceof Map) {
            //throw new IllegalArgumentException(DEFAULT_ERROR_MSG);
            log.warn(DEFAULT_ERROR_MSG);
        }
        return new Result<>(ResultCode.SUCCESS, data, null);
    }

    /**
     * 成功
     *
     * @param data 数据
     * @param msg  自定义信息
     * @param <T>  数据类型
     * @return 返回数据
     */
    public static <T> Result<T> success(T data, String msg) {
        if (data instanceof Map) {
            //throw new IllegalArgumentException(DEFAULT_ERROR_MSG);
            log.warn(DEFAULT_ERROR_MSG);
        }
        return new Result<>(ResultCode.SUCCESS.getCode(), StringUtils.isBlank(msg) ? ResultCode.SUCCESS.getMsg() : msg, data, null);
    }

    /**
     * 成功
     *
     * @param data  数据
     * @param total 数据条数
     * @param <T>   数据类型
     * @return 返回数据和数据条数
     */
    public static <T> Result<T> pageResult(T data, Long total) {
        if (total == null || total < 0) {
            throw new IllegalArgumentException("=======> total can't be null or min then 0");
        }
        if (data instanceof Map) {
            //throw new IllegalArgumentException(DEFAULT_ERROR_MSG);
            log.warn(DEFAULT_ERROR_MSG);
        }
        return new Result<>(ResultCode.SUCCESS, data, total);
    }

    /**
     * 成功
     *
     * @param msg   自定义信息
     * @param data  数据
     * @param total 数据条数
     * @param <T>   数据类型
     * @return 返回数据、自定义信息、数据条数
     */
    public static <T> Result<T> pageResult(String msg, T data, Long total) {
        if (total == null || total < 0) {
            throw new IllegalArgumentException("=======> total can't be null or min then 0");
        }
        if (data instanceof Map) {
            //throw new IllegalArgumentException(DEFAULT_ERROR_MSG);
            log.warn(DEFAULT_ERROR_MSG);
        }
        return new Result<>(ResultCode.SUCCESS.code, !StringUtils.isBlank(msg) ? ResultCode.SUCCESS.getMsg() : msg, data, total);
    }

    /**
     * 失败
     *
     * @param <T> 返回类型
     * @return 数据
     */
    public static <T> Result<T> failure() {
        return new Result<>(ResultCode.FAILURE_SERVICE, null, null);
    }

    /**
     * 失败
     *
     * @param code 返回码
     * @param msg  返回信息
     * @param <T>  返回类型
     * @return 数据
     */
    public static <T> Result<T> failure(int code, String msg) {
        return new Result<>(code, msg, null, null);
    }

    /**
     * 失败
     *
     * @param msg 自定义信息
     * @param <T> 返回类型
     * @return 数据
     */
    public static <T> Result<T> failure(String msg) {
        return new Result<>(ResultCode.FAILURE_SERVICE.getCode(), StringUtils.isNotBlank(msg) ? msg : ResultCode.FAILURE_SERVICE.getMsg(), null, null);
    }

    /**
     * 失败
     *
     * @param resultCode 自定义返回码(切记不要与已定义的错误码、成功码、失败码冲突，避免不必要的麻烦)
     * @param <T>        返回类型
     * @return 数据
     */
    public static <T> Result<T> failure(ResultCode resultCode) {
        return new Result<>(resultCode, null, null);
    }

    /**
     * 错误
     *
     * @param <T> 返回类型
     * @return 数据
     */
    public static <T> Result<T> error() {
        return new Result<>(ResultCode.ERROR_SERVICE, null, null);
    }

    /**
     * 错误
     *
     * @param msg 自定义返回信息
     * @param <T> 返回类型
     * @return 数据
     */
    public static <T> Result<T> error(String msg) {
        return new Result<>(ResultCode.ERROR_SERVICE.getCode(), StringUtils.isNotBlank(msg) ? msg : ResultCode.ERROR_SERVICE.getMsg(), null, null);
    }

    /**
     * 错误
     *
     * @param code 返回码
     * @param msg  返回信息
     * @param <T>  返回类型
     * @return 数据
     */
    public static <T> Result<T> error(int code, String msg) {
        return new Result<>(code, msg, null, null);
    }

    /**
     * 错误
     *
     * @param code 返回码
     * @param msg  返回信息
     * @param <T>  返回类型
     * @return 数据
     */
    public static <T> Result<T> error(int code, T data, String msg) {
        return new Result<>(code, msg, data, null);
    }

    /**
     * 错误
     *
     * @param resultCode 自定义返回码(切记不要与已定义的错误码、成功码、失败码冲突，避免不必要的麻烦)
     * @param <T>        返回类型
     * @return 数据
     */
    public static <T> Result<T> error(ResultCode resultCode) {
        return new Result<>(resultCode, null, null);
    }

    /**
     * 错误
     *
     * @param data 数据
     * @param <T>  返回类型
     * @return 数据
     */
    public static <T> Result<T> error(T data) {
        return new Result<>(ResultCode.ERROR, data, null);
    }

    /**
     * 错误
     *
     * @param data 数据
     * @param <T>  返回类型
     * @return 数据
     */
    public static <T> Result<T> error(T data, String msg) {
        return new Result<>(ResultCode.ERROR.getCode(), msg, data, null);
    }
}

```